
<?php
 if (isset($_POST['entreprise_raison_social']))
   {
     include "include/operation_message.php";  //---> inclure fonction pour afficher un message

      //---> il s'agit d'une modification
		  $entreprise_raison_social = lecture($_POST['entreprise_raison_social'] );
		  $entreprise_site = lecture($_POST['entreprise_site']) ;
		  $entreprise_email = lecture($_POST['entreprise_email']) ;
		  $entreprise_num_siret = lecture($_POST['entreprise_num_siret'] );
		  $entreprise_ref = lecture($_POST['entreprise_ref']) ;
		  $entreprise_domaine_activite = lecture($_POST['entreprise_domaine_activite'] );
 		  $entreprise_video_url = lecture($_POST['entreprise_video_url']) ;
		  $entreprise_date_modif = date("YY-mm-dd");
		  $entreprise_adresse = lecture($_POST['entreprise_adresse']) ;
		  $entreprise_code_postale = lecture($_POST['entreprise_code_postale']) ;
		  $entreprise_vile   = lecture($_POST['entreprise_vile']) ;
		  $entreprise_tel = lecture($_POST['entreprise_tel']) ;
		  $entreprise_description = utf8_encode(lecture($_POST['entreprise_description'])) ;


	     $sql = "UPDATE entreprise
	           SET     entreprise_raison_social             = '$entreprise_raison_social'        ,
			           entreprise_ref             			= '$entreprise_ref'        ,
					   entreprise_num_siret             	= '$entreprise_num_siret'        ,
					   entreprise_domaine_activite          = '$entreprise_domaine_activite'  ,
					   entreprise_email            			= '$entreprise_email'  ,
					   entreprise_site            			= '$entreprise_site' ,
						entreprise_video_url				= '$entreprise_video_url' ,
						entreprise_date_modif				= '$entreprise_date_modif' ,
						entreprise_adresse					= '$entreprise_adresse' ,
						entreprise_code_postale				= '$entreprise_code_postale' ,
						entreprise_vile						= '$entreprise_vile' ,
						entreprise_tel						= '$entreprise_tel' ,
						entreprise_description				= '$entreprise_description'
												WHERE  entreprise_id='$id_entreprise'
						";
		$res = $connexion->prepare($sql);
	$res->execute();

	   echo '<h2 class="skills">Modification(s) enregistrée(s)</h2>';

//---> Télécharger le fichier de l'image
	 if ($_FILES["entreprise_photo"]["size"] > 0 &&
	     $_FILES["entreprise_photo"]["error"]==0 &&
		 $_FILES["entreprise_photo"]["name"] != "")
	 {
	    //-->L'extension de l'image $ext
	   $filename   = $_FILES["entreprise_photo"]["name"];
	   $filename   = explode(".",$filename);
	   $ext        = $filename[count($filename)-1];

	   if($ext == 'jpg' || $ext == 'jpeg' || $ext == 'png' || $ext == 'gif'|| $ext == 'JPG'|| $ext == 'JPEG' || $ext == 'PNG' || $ext == 'GIF')
	   {

			$sql = " SELECT entreprise_photo FROM entreprise WHERE admin_user_id = $admin_user_id ";
			$res = $connexion->query($sql);
			$row  = $res->fetch();

			$entreprise_photo = $row['entreprise_photo'];
			@unlink("../common/Images/entreprise/$entreprise_photo");  //--> suppresssion de l'image pr&eacute;cedante

	   $filename   = "entreprise($admin_user_id).$ext";
	   upload_file("entreprise_photo", "../common/Images/entreprise/$filename");

	   //---> Je mets à jour le nom de l'image
       $sql = "UPDATE entreprise
	           SET    entreprise_photo   = '$filename'
			   WHERE  admin_user_id      = $admin_user_id";
       $res = $connexion->prepare($sql);
	  $res->execute();
 	   }
	   else
	   {
	   echo 'Format du fichier incorrect !';
	   }

	 } //Fsi

	  exit();
   }
?>
<?php

 $sql_entreprise_edit = "select * from entreprise where  entreprise_id='$id_entreprise'";
	$res_entreprise_edit = $connexion->query($sql_entreprise_edit);
	$donnees_entreprise_edit  = $res_entreprise_edit->fetch();

  $entreprise_raison_social = $donnees_entreprise_edit['entreprise_raison_social'] ;
  $entreprise_site = $donnees_entreprise_edit['entreprise_site'] ;
  $entreprise_email = $donnees_entreprise_edit['entreprise_email'] ;
  $entreprise_num_siret = $donnees_entreprise_edit['entreprise_num_siret'] ;
  $entreprise_ref = $donnees_entreprise_edit['entreprise_ref'] ;
  $entreprise_domaine_activite = $donnees_entreprise_edit['entreprise_domaine_activite'] ;
  $entreprise_photo = $donnees_entreprise_edit['entreprise_photo'] ;							//./common/Images/entreprise/
  $entreprise_video_url = $donnees_entreprise_edit['entreprise_video_url'] ;
  $entreprise_date_modif = date("YY-mm-dd");
  $entreprise_adresse = $donnees_entreprise_edit['entreprise_adresse'] ;
  $entreprise_code_postale = $donnees_entreprise_edit['entreprise_code_postale'] ;
  $entreprise_vile   = $donnees_entreprise_edit['entreprise_vile'] ;
  $entreprise_tel = $donnees_entreprise_edit['entreprise_tel'] ;
  $entreprise_description = utf8_encode($donnees_entreprise_edit['entreprise_description']) ;

?>

<section class="cv-edit clearfix">
	<h3>Éffectuer l'édition de votre profil :</h3>
	<form   action="" method="post" name="user_modif"  id='user_modif' enctype="multipart/form-data">

	<div class="col-tab">
		<p class="titleInterest">Raison Social</p>
		<section class="filter clearfix">
			<input type="text" class="reference" for="name"  placeholder="Nom de l'entreprise" name="entreprise_raison_social" id="entreprise_raison_social"  value="<?php echo $entreprise_raison_social?>"/>

		</section>
			<p class="titleInterest">Votre principal domaine de compétence</p>
		<section class="filter clearfix">
			<input type="text" class="reference" for="competence"  placeholder="Votre compétence principale" name="entreprise_domaine_activite" id="entreprise_domaine_activite"  value="<?php echo $entreprise_domaine_activite?>"/>

		</section>
		<section class="filter clearfix">
			<p class="titleInterest">entreprise ref </p>
			<input type="text" class="reference" for="competence"  placeholder="Votre Ref" name="entreprise_ref" id="entreprise_ref"  value="<?php echo $entreprise_ref?>"/>

		</section>
		<section class="filter clearfix">
			<p class="titleInterest">Votre numéro de téléphone</p>
			<input type="text" class="reference" for="competence"  placeholder="Votre téléphone" name="entreprise_tel" id="entreprise_tel"  value="<?php echo $entreprise_tel?>"/>

		</section>
		<section class="filter clearfix">form
			<p class="titleInterest">Votre numéro de siret</p>
			<input type="text" class="reference" for="competence"  placeholder="Votre siret" name="entreprise_num_siret" id="entreprise_num_siret"  value="<?php echo $entreprise_num_siret?>"/>

		</section>
		<section class="filter clearfix">
			<p class="titleInterest">Votre email</p>
			<input type="text" class="reference" for="mail"  placeholder="Votre  adresse mail " name="entreprise_email" id="entreprise_email"  value="<?php echo $entreprise_email?>"/>

		</section>
		<section class="filter clearfix">
			<p class="titleInterest">Votre site web </p>
			<input type="text" class="reference" for="competence"  placeholder="Votre  site web " name="entreprise_site" id="entreprise_site"  value="<?php echo $entreprise_site?>"/>

		</section>
	</div>
	<div class="col-tab">
		<section class="filter clearfix">
			<p class="titleInterest">Votre logo</p>
			<?php if($entreprise_photo!="")

				{
					?>
					  <img src="./common/Images/entreprise/<?php  echo $entreprise_photo?>" width="80"    style="padding-left:5px; float: right;" border="0" align="absmiddle" alt="t&eacute;l&eacute;chargement" />
					<?php
				}
				else
				{
					?>
					  <img src="./common/Images/vide.gif" width="80"    style="padding-left:5px; float: right;" border="0" align="absmiddle" alt="t&eacute;l&eacute;chargement" />
					<?php
				}

					?>
			<input type="file" class="reference_image" for="competence"  placeholder="Télécharger votre logo" name="entreprise_photo" id="entreprise_photo" />

		</section>
		<section class="filter clearfix">
			<p class="titleInterest">Votre vidéo</p>
			<input type="text" class="reference" for="competence"  placeholder="Url de votre vidéo" name="entreprise_video_url" id="entreprise_video_url"  value="<?php echo $entreprise_video_url?>"/>

		</section>
		<section class="filter clearfix skill">
			<p class="titleInterest">Votre Description</p>
			<textarea class="reference_textarea" for="competence"  placeholder="entreprise description" name="entreprise_description" id="entreprise_description"   rows="52"><?php echo $entreprise_description?></textarea>
 		</section>
		<section class="filter clearfix skill">
		<p class="titleInterest">Votre Adresse</p>
			<input type="text" class="reference" for="competence"  placeholder="entreprise_adresse" name="entreprise_adresse" id="entreprise_adresse"  value="<?php echo $entreprise_adresse?>" />

			<input type="text" class="reference_code_postale" for="competence"  placeholder="entreprise_code_postale" name="entreprise_code_postale" id="entreprise_code_postale"  value="<?php echo $entreprise_code_postale?>" />

			<input type="text" class="reference_code_vile" for="competence"  placeholder="entreprise_vile" name="entreprise_vile" id="entreprise_vile"  value="<?php echo $entreprise_vile?>" />

		</section>
	</div>

	<input type="submit" class="submit valid valid-profil" for="enregistrer" value="enregistrer"/>
	</form>
</section>