<!DOCTYPE html>
<!--[if IE 7]><html class="ie ie7" lang="fr-FR"><![endif]-->
<!--[if IE 8]><html class="ie ie8" lang="fr-FR"><![endif]-->
<!--[if !(IE 7) | !(IE 8)  ]><!-->
<!--<![endif]-->
<html lang="fr-FR">

<head>
	<meta charset="UTF-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="IE=edge, chrome=1">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>cvweb | jobvideo.fr &#124; Le CV vidéo &#124; abonnements</title>
	<link rel="profile" href="http://gmpg.org/xfn/11" />
	<link rel="shortcut icon" href="/images/favicon.ico" type="image/x-icon">
	<link rel="icon" href="/images/favicon.ico" type="image/x-icon">
	<link rel="stylesheet" type="text/css" media="all" href="./css/styles.css" />
	<link href='http://fonts.googleapis.com/css?family=Roboto:400,700' rel='stylesheet' type='text/css'>
	<link href='http://fonts.googleapis.com/css?family=Titillium+Web:400,700' rel='stylesheet' type='text/css'>
	<link href='http://fonts.googleapis.com/css?family=Nunito:700' rel='stylesheet' type='text/css'>
	<link rel="pingback" href="http://jobvideo.fr/xmlrpc.php" />
	<!--[if gte IE 9]><style type="text/css">.gradient {filter: none;}</style><![endif]-->
	<!--[if lt IE 9]><script src="scripts/html5shiv.js"></script><![endif]-->
	<!--[if IE]><link rel="stylesheet" type="text/css" href="css/ie.css" media="screen"><![endif]-->
	<!--[if lt IE 9]><style>.content{height: auto; margin: 0;}	.content div {position: relative;}</style><![endif]-->
	<script>var max_nb=0;</script>  
 
</head>
<body id="body" class="page cv-entreprises">
	<div class="overlay" id="overlay"></div>
	<main id="main" role="main">
		<div class="btn_up"></div>
		<header id="header" class="header clearfix">
			<nav id="nav-widget" class="nav-widget clearfix">
				<div class="wrapper">
					<ul class="breadcrumbs">
						<li><a class="home-icon" href="http://jobvideo.fr">Accueil</a></li>
						<li class="separator"> > </li>
						<li>Vous êtes ici : prévoir ici un breadcrumb</li>
					</ul>
					 <?php
 $id_entreprise = $_SESSION['id_entreprise'];
  $sql=mysql_query(" select distinct  id_candidat from panier where id_entreprise='$id_entreprise' ");
$nb_panier = mysql_num_rows($sql) ;
 ?>
					<div id="widget-panier" class="widget-panier clearfix">
						<a class="bucket" href="./index.php?p=panier&session=<?php  echo $idses?>" title="voir mon panier">panier</a><span style="color:#ff0000 !important;">(<?php echo $nb_panier; ?>)</span>
					</div>
				</div>
				 
			</nav>
			<nav id="nav-header" class="nav-header wrapper clearfix">
				<div id="nav-inner" class="navInner">
					<a href="#" id="nav-Responsive" class="navResponsive" title="ouvrir la navigation"><span class="menuBurger">ouvrir la navigation</span></a>
					<a id="logo" class="logo" href="http://jobvideo.fr/" title="retour à l'accueil">jobvideo.fr</a>
				</div>
				<div id="nav-top" class="menu-entreprises-container navTop">
					<ul id="menu-entreprises" class="menu-header clearfix">
						<li id="menu-item-123" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-123">
							<a href="http://jobvideo.fr/deontologie/">Déontologie</a>
						</li>
						<li id="menu-item-124" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-124">
							<a href="http://jobvideo.fr/historique/">Historique</a>
						</li>
						<li id="menu-item-128" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-128">
							<a title="pour nous contacter" href="http://jobvideo.fr/#featured-contact">contactez-nous</a>
						</li>
						<li id="menu-item-124" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-124">
							<a href="./?p=index&session=<?php echo $idses?>&ferms=1">Log out </a>
						</li>
					</ul>
				</div>
			</nav>
		</header>