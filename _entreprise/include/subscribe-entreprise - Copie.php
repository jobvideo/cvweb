<ul class="cd-pricing clearfix">
    <li class="abo1 abo clearfix">
        <div class="cd-pricing-header1">
            <h2>BASIC</h2>
            <div class="cd-price">
                <span>15€/an</span>
            </div>
        </div> <!-- .cd-pricing-header -->
        <div class="cd-pricing-features">
            <ul>
                <li>Accès au moteur de recherche</li>
                <li>Accès au profil partiel (vidéo et synthèse)</li>
                <li>Achat de profil candidat</li>
            </ul>
            <p>1 profil complet gratuit / an</p>
        </div> <!-- .cd-pricing-features -->
        <div class="cd-pricing-footer">
            <a href="./?p=choisir&val=0&session=<?php echo $idses?>">Choisir</a>
        </div>
    </li>
    <li class="abo2 abo">
        <div class="cd-pricing-header2">
            <h2>PREMIUM 1</h2>
            <div class="cd-price">
                <span>90€/an</span>
            </div>
        </div> <!-- .cd-pricing-header -->
        <div class="cd-pricing-features">
            <ul>
                <li>Accès au moteur de recherche</li>
                <li>Accès au profil complet (vidéo, synthèse et coordonnées)</li>
                <li>Achat de profil candidat</li>
            </ul>
            <p>5 profils complets gratuits / an</p>
        </div> <!-- .cd-pricing-features -->
        <div class="cd-pricing-footer">
            <a href="./?p=choisir&val=1&session=<?php echo $idses?>">Choisir</a>
        </div>
    </li>
    <li class="abo3 abo">
        <div class="cd-pricing-header3">
            <h2>PREMIUM 2</h2>
            <div class="cd-price">
                <span>150€/an</span>
            </div>
        </div> <!-- .cd-pricing-header -->
        <div class="cd-pricing-features">
            <ul>
                <li>Accès au moteur de recherche</li>
                <li>Accès au profil complet (vidéo, synthèse et coordonnées)</li>
                <li>Achat de profil candidat</li>
            </ul>
            <p>10 profils complets gratuits / an</p>
        </div> <!-- .cd-pricing-features -->
        <div class="cd-pricing-footer">
            <a href="./?p=choisir&val=2&session=<?php echo $idses?>">Choisir</a>
        </div>
    </li>
    <li class="abo4 abo">
        <div class="cd-pricing-header4">
            <h2>PREMIUM 3</h2>
            <div class="cd-price">
                <span>180€/an</span>
            </div>
        </div> <!-- .cd-pricing-header -->
        <div class="cd-pricing-features">
            <ul>
                <li>Accès au moteur de recherche</li>
                <li>Accès au profil complet (vidéo, synthèse et coordonnées)</li>
                <li>Achat de profil candidat</li>
            </ul>
            <p>15 profils complets gratuits / an</p>
        </div> <!-- .cd-pricing-features -->
        <div class="cd-pricing-footer">
            <a href="./?p=choisir&val=3&session=<?php echo $idses?>">Choisir</a>
        </div>
    </li>
    <li class="abo5">
        <div class="cd-pricing-header5">
            <h2>PROFIL COMPLET</h2>
            <div class="cd-price">
                <span>20€/unité</span>
            </div>
        </div> <!-- .cd-pricing-header -->
        <div class="cd-pricing-footer">
            <a href="./?p=choisir&val=4&session=<?php echo $idses?>">Choisir</a>
        </div>
    </li>
</ul>
<div class="cd-form">
    <div class="cd-plan-info">
        <!-- content will be loaded using jQuery - according to the Choisired plan -->
    </div>
    <div class="cd-more-info">
        <p class="title-contact"><strong>En savoir plus ?</strong></p>
        <p>jobvideo.fr - BP 20012 - 76301 Sotteville-lès-Rouen CEDEX</p>
        <p class="title-contact"><strong>Tél. 00 00 00 00 00</strong></p>
    </div>
    <form class="clearfix" action="">
        <fieldset class="info-count clearfix">
            <legend>Information de votre compte</legend>
            <div class="half-width">
                <label for="userName">Nom</label>
                <input type="text" id="userName" name="userName">
            </div>
            <div class="half-width">
                <label for="userEmail">Email</label>
                <input type="email" id="userEmail" name="userEmail">
            </div>
            <div class="half-width">
                <label for="userPassword">Mot de passe</label>
                <input type="password" id="userPassword" name="userPassword">
            </div>
            <div class="half-width">
                <label for="userPasswordRepeat">Répéter le mot de passe</label>
                <input type="password" id="userPasswordRepeat" name="userPasswordRepeat">
            </div>
        </fieldset>
        <fieldset class="payment-gateway clearfix">
            <legend>Méthode de paiement</legend>
            <div>
                <ul class="cd-payment-gateways">
                    <li>
                        <input type="radio" name="payment-method" id="paypal" value="paypal">
                        <label for="paypal">Paypal</label>
                    </li>

                    <li>
                        <input type="radio" name="payment-method" id="card" value="card" checked>
                        <label for="card">Carte Bleue</label>
                    </li>
                </ul> <!-- .cd-payment-gateways -->
            </div>
            <div class="cd-credit-card">
                <div>
                    <p class="third-width">
                        <label for="cardNumber">Numéro de carte</label>
                        <input type="text" id="cardNumber" name="cardNumber">
                    </p>
                    <p class="third-width credit-date">
                        <label>date d'expiration</label>
                        <b>
                            <span class="cd-select">
                                <select name="card-expiry-month" id="card-expiry-month">
                                    <option value="1">1</option>
                                    <option value="1">2</option>
                                    <option value="1">3</option>
                                    <option value="1">4</option>
                                    <option value="1">5</option>
                                    <option value="1">6</option>
                                    <option value="1">7</option>
                                    <option value="1">8</option>
                                    <option value="1">9</option>
                                    <option value="1">10</option>
                                    <option value="1">11</option>
                                    <option value="1">12</option>
                                </select>
                            </span>

                            <span class="cd-select">
                                <select name="card-expiry-year" id="card-expiry-year">
                                    <option value="2015">2015</option>
                                    <option value="2015">2016</option>
                                    <option value="2015">2017</option>
                                    <option value="2015">2018</option>
                                    <option value="2015">2019</option>
                                    <option value="2015">2020</option>
                                </select>
                            </span>
                        </b>
                    </p>
                    <p class="third-width">
                        <label for="cardCvc">Code de sécurité</label>
                        <input type="text" id="cardCvc" name="cardCvc">
                    </p>
                </div>
            </div> <!-- .cd-credit-card -->
        </fieldset>
        <fieldset>
            <div>
                <input methode="POST" type="submit" value="S'abonner" action="panier.php">
            </div>
        </fieldset>
    </form>
    <a href="#0" class="cd-close"></a>
</div> <!-- .cd-form -->
<div class="cd-overlay"></div> <!-- shadow layer -->