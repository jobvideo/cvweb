 <form class="clearfix" action="">
        <fieldset class="info-count clearfix">
            <legend>Information de votre compte</legend>
            <div class="half-width">
                <label for="userName">Nom</label>
                <input type="text" id="userName" name="userName">
            </div>
            <div class="half-width">
                <label for="userEmail">Email</label>
                <input type="email" id="userEmail" name="userEmail">
            </div>
            <div class="half-width">
                <label for="userPassword">Mot de passe</label>
                <input type="password" id="userPassword" name="userPassword">
            </div>
            <div class="half-width">
                <label for="userPasswordRepeat">Répéter le mot de passe</label>
                <input type="password" id="userPasswordRepeat" name="userPasswordRepeat">
            </div>
        </fieldset>
        <fieldset class="payment-gateway clearfix">
            <legend>Méthode de paiement</legend>
            <div>
                <ul class="cd-payment-gateways">
                    <li>
                        <input type="radio" name="payment-method" id="paypal" value="paypal">
                        <label for="paypal">Paypal</label>
                    </li>

                    <li>
                        <input type="radio" name="payment-method" id="card" value="card" checked>
                        <label for="card">Carte Bleue</label>
                    </li>
                </ul> <!-- .cd-payment-gateways -->
            </div>
            <div class="cd-credit-card">
                <div>
                    <p class="third-width">
                        <label for="cardNumber">Numéro de carte</label>
                        <input type="text" id="cardNumber" name="cardNumber">
                    </p>
                    <p class="third-width credit-date">
                        <label>date d'expiration</label>
                        <b>
                            <span class="cd-select">
                                <select name="card-expiry-month" id="card-expiry-month">
                                    <option value="1">1</option>
                                    <option value="1">2</option>
                                    <option value="1">3</option>
                                    <option value="1">4</option>
                                    <option value="1">5</option>
                                    <option value="1">6</option>
                                    <option value="1">7</option>
                                    <option value="1">8</option>
                                    <option value="1">9</option>
                                    <option value="1">10</option>
                                    <option value="1">11</option>
                                    <option value="1">12</option>
                                </select>
                            </span>

                            <span class="cd-select">
                                <select name="card-expiry-year" id="card-expiry-year">
                                    <option value="2015">2015</option>
                                    <option value="2015">2016</option>
                                    <option value="2015">2017</option>
                                    <option value="2015">2018</option>
                                    <option value="2015">2019</option>
                                    <option value="2015">2020</option>
                                </select>
                            </span>
                        </b>
                    </p>
                    <p class="third-width">
                        <label for="cardCvc">Code de sécurité</label>
                        <input type="text" id="cardCvc" name="cardCvc">
                    </p>
                </div>
            </div> <!-- .cd-credit-card -->
        </fieldset>
        <fieldset>
            <div>
                <input methode="POST" type="submit" value="S'abonner" action="panier.php">
            </div>
        </fieldset>
    </form>
   