<?php //include ('include/header_candidat.php'); ?>
<?php
	
	$cand= $_GET['cnd'];
	$id_entreprise = $_SESSION['id_entreprise'] ;
	$sql_acheter	= "SELECT * FROM  acheter  WHERE  id_entreprise  ='$id_entreprise' and id_candidat ='$cand' and valide='Y'";
	$res_acheter = $connexion->query($sql_acheter);
 
	 
	 $nb_acht = $res_acheter->rowCount();
	 
	if($nb_acht >0)
	{
		$sql_entreprise = "select * from entreprise where 	admin_user_id='$id_entreprise'";
		$res_entreprise = $connexion->query($sql_entreprise);
		$donnees_entreprise  = $res_entreprise->fetch();
 		$nom = $donnees_entreprise['entreprise_raison_social'] ;
		
		$sql_voir	= "SELECT * FROM   candidat,ville,metier  WHERE  candidat_id  ='$cand' and  candidat.ville_id = ville.ville_id and  candidat.metier_id = metier.metier_id";
		$res_voir = $connexion->query($sql_voir);
		$row_voir  = $res_voir->fetch();
		 
		$sql_experience	= "SELECT * FROM   experience  WHERE  candidat_id  ='$cand' ";
		$res_experience   = $connexion->query($sql_experience);
		 
		$sql_competence	= "SELECT * FROM   graphic_competence  WHERE  candidat_id  ='$cand' ";
		$res_competence = $connexion->query($sql_competence);
 		
		$candidat_date_dispo = $row_voir['candidat_date_dispo'];
		$candidat_ref = $row_voir['candidat_ref'];
		$candidat_photo = $row_voir['candidat_photo'];  
		$candidat_id = $row_voir['candidat_id'] ;
		$candidat_nom = utf8_encode($row_voir['candidat_nom'].' -  '.$row_voir['candidat_prenom']) ;
		$candidat_profession =utf8_encode(  $row_voir['metier_nom'])  ;
		$ville= $row_voir['ville_nom']." ".$row_voir['candidat_codepostale'] ; 
						 
		$candidat_prenom = utf8_encode($row_voir['candidat_prenom']);
		$candidat_date_naissance = $row_voir['candidat_date_naissance'];
		$candidat_sex= $row_voir['candidat_sex'];
		$candidat_permis= $row_voir['candidat_permis'];
				$cadidant_temps_rechercher= $row_voir['cadidant_temps_rechercher'];

		$candidat_mobilite= $row_voir['candidat_mobilite'];
		$candidat_moyen_transport= $row_voir['candidat_moyen_transport'];
		$candidat_hanicap= $row_voir['candidat_hanicap'];
		$candidat_dispo= $row_voir['candidat_dispo'];
		$candidat_date_dispo= $row_voir['candidat_date_dispo'];
		$candidat_ref= $row_voir['candidat_ref'];
		$candidat_profession= $row_voir['candidat_profession'];
		$candidat_nbr_experience= $row_voir['candidat_nbr_experience'];
		$candidat_position= $row_voir['candidat_position'];
		$candidat_photo= $row_voir['candidat_photo'];
		$candidat_visible= $row_voir['candidat_visible'];
		$candidat_une= $row_voir['candidat_une'];
		$candidat_telephone= $row_voir['candidat_telephone'];
		$candidat_email= $row_voir['candidat_email'];
		$candidat_domicilite= $row_voir['candidat_domicilite'];
	 
		$candidat_handicap= $row_voir['candidat_handicap'];
		$candidat_emploi_rechercher= $row_voir['candidat_emploi_rechercher'];
		$candidat_diplome= $row_voir['candidat_diplome'];
		$candidat_certification= $row_voir['candidat_certification'];
		$candidat_lettre_motivation= $row_voir['candidat_lettre_motivation'];
		$candidat_synthese= $row_voir['candidat_synthese'];
		$candidat_video= $row_voir['candidat_video'];
		$candidat_cv= $row_voir['candidat_cv'];
		$candidat_attestation= $row_voir['candidat_attestation'];
		$candidat_diplome2= $row_voir['candidat_diplome2'];
		$candidat_certificat= $row_voir['candidat_certificat'];
		
		
		
		
		
		
		
		
?>
 
    <section id="featured-log" class="featured clearfix">
        <article id="intro-profil" role="section" class="intro-profil clearfix">
            <div id="image-featured" class="image-featured">

                <h1>Bienvenue dans l'espace candidat</h1>  
				<h3><?php echo $candidat_nom; ?></h3>
                <p>Ici vous pouvez trouver le profil candidat complet.</p>
            </div>
        </article>

        <article id="profil" role="section" class="clearfix">
            <div id="cv" class="cv clearfix">
                <div id="cv-content" class="cv-content">
                    <article class="cv-content-inner clearfix">
                        <figure id="portrait" class="portrait">
                            <img src="./common/Images/candidat/<?php echo $candidat_photo; ?>" alt="">
                        </figure>

                        <div class="cv-text">
                            <h2><strong><?php echo $candidat_nom; ?> / <?php echo $candidat_prenom; ?></strong></h2>

                            <h2><?php echo $candidat_profession; ?></h2>

                            <h3><?php echo $ville; ?></h3>

                            <ul id="ref" class="ref">
                                <li>Candidat disponible à partir du : <?php echo $candidat_date_dispo; ?></li>

                                <li>Ref. <?php echo $candidat_ref; ?></li>
                            </ul>
                        </div>
                    </article>
                </div>

                <div id="cv-tabs" class="cv-tabs">
                    <div class="cv-tabs-inner clearfix">
                        
                        <ul id="tabs" class="tabs clearfix">
                            <li>
                                <a class="tab1" id="firstonglet" href="#tabs-1" title="Présentation du candidat">Présentation</a>
                            </li>

                            <li>
                                <a class="tab2" href="#tabs-2" title="Voir les compétences du candidat">Compétences</a>
                            </li>

                            <li>
                                <a class="tab3" href="#tabs-3" title="Consulter la synthése RH du candidat">Synthése</a>
                            </li>

                            <li>
                                <a class="tab4" href="#tabs-4" title="Télécharger les documents professionnels du candidat">Documents</a>
                            </li>

                            <li>
                                <a class="tab5" id="ssborder" href="#tabs-5" title="Voir l'ensemble des données du candidats">Résumé</a>
                            </li>
                        </ul>
                    </div>

                    <div id="content-tab" class="content-tab">
                        <div id="tabs-1" class="clearfix tab pres">
                            <p class="maj">Derniére mise à jour : le 22/03/2016 à 15 h 27 min</p>

                            <div id="left-col" class="left-col">
                                <p class="tab-head">vidéo</p>

                                <div class="embed-container">
                                    <iframe width="200" height="113" src="<?php echo $candidat_video;?>" frameborder="0" allowfullscreen=""></iframe>
                                </div>
                            </div><!--#left-col-->

                            <div id="right-col" class="right-col">
                                <div class="head">
                                    <p class="tab-head">expériences</p>
                                  <?php //boucle experience // ?>
                                    <ul class="tab-cell">
                                        <li class="exp">Formateur-consultant</li>

                                        <li class="exp">Agent d'exploitation aérien</li>
                                    </ul>
                                </div><!--.head-->
                                <!--
                                <div class="head head-right">
                                    <p class="tab-head">synthése rh</p>
                                    <p class="tab-cell synt">Cliquez <a href="http://jobvideo.fr/wp-content/uploads/2015/03/modele-synthese-rh.pdf" target="_blank">ici</a> pour télécharger</p>
                                </div>
        --><!--.head-->

                                <div class="list-caract">
                                    <ul class="tab-cell caract">
                                        <li><strong>diplome</strong><?php $candidat_diplome; ?></li>

                                        <li><strong>mobilité</strong><?php $candidat_mobilite; ?></li>

                                        <li><strong>permis de conduire</strong><?php $candidat_permis; ?></li>

                                        <li><strong>recherche</strong><?php $cadidant_temps_rechercher; ?></li>

                                        <li><strong>moyen de transport</strong> <?php $candidat_moyen_transport; ?></li>
                                    </ul>
                                </div><!--.list-caract-->
                            </div><!--#right-col-->
                        </div><!--#tabs-1-->

                        <div id="tabs-2" class="clearfix tab competences">
                            <script type="text/javascript">
var mydata =[] ;
                                $(function () {
                                // CRéER LE GRAPHIQUE
                                $('#chartt').highcharts({
                                chart: {
                                type: 'column',
                                events: {
                                drilldown: function (e) {
                                if (!e.seriesOptions) {
                                // Show the loading label
                                chart.showLoading('Simulating Ajax ...');
                                setTimeout(function () {
                                chart.hideLoading();
                                chart.addSeriesAsDrilldown(e.point, series);
                                }, 1000);
                                }
                                }
                                }
                                },
                                colors:['#64C1D1', '#64C1D1', '#64C1D1', '#64C1D1', '#64C1D1', '#64C1D1', '#64C1D1', '#64C1D1', '#813B71', '#813B71', '#E1312C'],
                                credits:{
                                enabled:false
                                },
                                title: {
                                text: ''
                                },
                                labels: {
                                style:{
                                "color" : "#1B191A"
                                }
                                },
                                xAxis: {
                                type: 'category',
                                style:{
                                "color" : "##1B191A"
                                }
                                },
                                yAxis:{
                                title: {
                                text: 'Niveau'
                                },
                                style:{
                                "color" : "##1B191A"
                                }
                                },
                                legend: {
                                enabled: false
                                },
                                plotOptions: {
                                series: {
                                borderWidth: 0,
                                dataLabels: {
                                enabled: false
                                }
                                }
                                },
                                series: [{
                                name: 'compétences',
                                colorByPoint: true,
                                data: mydata
                                }],
                                drilldown: {
                                series: []
                                }
                                });
                                });
                            </script>
                            <?php
							//comp
							?>
                            <div id="left-col" class="left-col">
                                <p class="tab-head tab-tech">Compétences techniques</p>

                                <ul class="tab-cell comptech">
                                    <li><strong>comp 1</strong>Définit un contenu pédagogique</li>

                                    <li style="list-style: none; display: inline">
                                        <script type="text/javascript">
mydata.push({
                                            name: 'COMP '+ 1,
                                            y: 5,
                                            drilldown: true
                                            });
                                        </script>
                                    </li>

                                    <li><strong>comp 2</strong>Transmet des savoirs et des savoir faire inscrits dans une progression pédagogique au moyen de techniques éducatives appropriées</li>

                                    <li style="list-style: none; display: inline">
                                        <script type="text/javascript">
mydata.push({
                                            name: 'COMP '+ 2,
                                            y: 5,
                                            drilldown: true
                                            });
                                        </script>
                                    </li>

                                    <li><strong>comp 3</strong>Manipule et met en éuvre les outils, matériels et équipements dans le cadre déapplications pédagogiques</li>

                                    <li style="list-style: none; display: inline">
                                        <script type="text/javascript">
mydata.push({
                                            name: 'COMP '+ 3,
                                            y: 5,
                                            drilldown: true
                                            });
                                        </script>
                                    </li>

                                    <li><strong>comp 4</strong>Participe é léélaboration des programmes de formation</li>

                                    <li style="list-style: none; display: inline">
                                        <script type="text/javascript">
mydata.push({
                                            name: 'COMP '+ 4,
                                            y: 4,
                                            drilldown: true
                                            });
                                        </script>
                                    </li>

                                    <li><strong>comp 5</strong>Diffuse de léinformation auprés des publics</li>

                                    <li style="list-style: none; display: inline">
                                        <script type="text/javascript">
mydata.push({
                                            name: 'COMP '+ 5,
                                            y: 5,
                                            drilldown: true
                                            });
                                        </script>
                                    </li>

                                    <li><strong>comp 6</strong>Entretient des relations professionnelles avec léenvironnement institutionnel et professionnel</li>

                                    <li style="list-style: none; display: inline">
                                        <script type="text/javascript">
mydata.push({
                                            name: 'COMP '+ 6,
                                            y: 4,
                                            drilldown: true
                                            });
                                        </script>
                                    </li>

                                    <li><strong>comp 7</strong>évalue les parcours individuels et en assure le suivi</li>

                                    <li style="list-style: none; display: inline">
                                        <script type="text/javascript">
mydata.push({
                                            name: 'COMP '+ 7,
                                            y: 5,
                                            drilldown: true
                                            });
                                        </script>
                                    </li>

                                    <li><strong>comp 8</strong>établit les bilans et rapports de stages</li>

                                    <li style="list-style: none; display: inline">
                                        <script type="text/javascript">
mydata.push({
                                            name: 'COMP '+ 8,
                                            y: 5,
                                            drilldown: true
                                            });
                                        </script>
                                    </li>
                                </ul>

                                <p class="tab-head tab-assoc">Compétences associées</p>

                                <ul class="tab-cell compasso">
                                    <li><strong>comp 9</strong>Connaét léenvironnement socio économique de léentreprise et son fonctionnement, les pratiques professionnelles du marché et leurs évolutions</li>

                                    <li style="list-style: none; display: inline">
                                        <script type="text/javascript">
mydata.push({
                                            name: 'COMP '+9,
                                            y: 4,
                                            drilldown: true
                                            });
                                        </script>
                                    </li>

                                    <li><strong>comp 10</strong>Utilise des équipements informatiques (EAO, multimédias, systémes experts...</li>

                                    <li style="list-style: none; display: inline">
                                        <script type="text/javascript">
mydata.push({
                                            name: 'COMP '+10,
                                            y: 5,
                                            drilldown: true
                                            });
                                        </script>
                                    </li>
                                </ul>

                                <p class="tab-head tab-supp">Compétence(s) supplémentaire(s)</p>

                                <ul class="tab-cell compsupp">
                                    <li><strong>comp 11</strong>évalue des populations avec des troubles des apprentissages</li>

                                    <li style="list-style: none; display: inline">
                                        <script type="text/javascript">
mydata.push({
                                            name: 'COMP '+11,
                                            y: 5,
                                            drilldown: true
                                            });
                                        </script>
                                    </li>
                                </ul>
                            </div><!--#left-col-->

                            <div id="right-col" class="right-col">
                                <p class="tab-head othertest">Compétences | graphique</p>

                                <div class="chartt" id="chartt" style="min-width: 310px; height: 400px; margin: 0 auto"></div><legend><strong>0</strong>Compétence non acquise</legend> <legend><strong>1</strong>Compétence uniquement acquise sur le plan théorique</legend> <legend><strong>2</strong>Compétence trés peu pratiquée</legend> <legend><strong>3</strong>Compétence mise parfois en pratique</legend> <legend><strong>4</strong>Compétence souvent pratiquée</legend> <legend><strong>5</strong>Compétence mise en pratique é long terme</legend>
                            </div><!--#right-col-->
                        </div><!--#tabs-2=-->

                        <div id="tabs-3" class="clearfix tab quest">
                            <script type="text/javascript">
var myvalue =[] ;
                                var mycategories=[] ;
                                $(function () {
                                $('#graphique_personnalite').highcharts({
                                chart: {
                                polar: true,
                                type: 'area'
                                },
                                credits:{
                                enabled:false
                                },
                                title: {
                                text: ''
                                },
                                xAxis: {
                                categories: mycategories
                                },
                                series: [{
                                name: 'Personnalité',
                                "color" : "#813B71",
                                data: myvalue
                                }]
                                });
                                });
                            </script>

                            <div id="left-col" class="left-col">
                                <p class="tab-head">synthése rh</p>

                                <p class="tab-cell synt">Cliquez <a href="http://jobvideo.fr/wp-content/uploads/2015/03/modele-synthese-rh.pdf" target="_blank">ici</a> pour télécharger</p>

                                <p>Nos outils vous permettent de mieux connaétre vos futurs collaborateurs, de cerner davantage leurs attentes, leur motivation et leurs compétences.</p>

                                <p class="tab-head tab-assoc">Personnalité au travail</p>

                                <p>évaluez le profil professionnel de nos candidats, leur adéquation au(x) poste(s); Une synthése de ces résultats vous est proposée. Profil Pro R est un test développé par Central Test.</p>

                                <p class="tab-head tab-supp">Motivations</p>

                                <p>Découvrez les principales sources de motivations professionnelles.</p>

                                <p class="tab-head">Aptitudes</p>

                                <p>Appréciez les qualités professionnelles et le savoir-faire de nos candidats. L'analyses des tests. L'analyse de nos tests est effectuée par des professionnels des ressources humaines diplémés en psychologie du travail. Ces tests ne font pas léobjet de préparations spécifiques. Ils témoignent de tendances observées au moment de léentretien. Ces données sont dynamiques et peuvent évoluer selon des événements liés é la vie professionnelle ou personnelle du candidat.</p>

                                <p class="tab-head othertest">Besoin déautres tests</p>

                                <p>Vous avez retenu un candidat mais certains doutes persistent ? Déautres tests peuvent vous étre proposés : Test de situation au travail, test déaptitude, test de connaissances...</p>

                                <p class="submit-button"><a class="submit" href="/#featured-contact">Contactez-nous</a></p>
                            </div><!--#left-col-->

                            <div id="right-col" class="right-col">
                                <p class="tab-head tab-assoc">Personnalité au travail | graphique</p><script type="text/javascript">
myvalue.push(2);
                                    mycategories.push('Volonté de persuasion');
                                </script> <script type="text/javascript">
myvalue.push(7);
                                    mycategories.push('Empathie/flexibilité');
                                </script> <script type="text/javascript">
myvalue.push(7);
                                    mycategories.push('Résistance au stress');
                                </script> <script type="text/javascript">
myvalue.push(2);
                                    mycategories.push('Extraversion');
                                </script> <script type="text/javascript">
myvalue.push(8);
                                    mycategories.push('Adaptation/improvisation');
                                </script> <script type="text/javascript">
myvalue.push(7);
                                    mycategories.push('Rationalisme');
                                </script> <script type="text/javascript">
myvalue.push(7);
                                    mycategories.push('Implication personnelle');
                                </script> <script type="text/javascript">
myvalue.push(2);
                                    mycategories.push('Volonté de pouvoir');
                                </script> <script type="text/javascript">
myvalue.push(8);
                                    mycategories.push('Besoin d'action');
                                </script> <script type="text/javascript">
myvalue.push(5);
                                    mycategories.push('Ambition');
                                </script>

                                <div class="chartt" id="graphique_personnalite" style="height: 400px"></div>

                                <p class="tab-head tab-supp">Motivations | graphique</p><script type="text/javascript">
var lavaleur =[] ;
                                    var lacategorie=[] ;
                                    $(function () {
                                    $('#graphique_motivations').highcharts({
                                    chart: {
                                    polar: true,
                                    type: 'area'
                                    },
                                    credits:{
                                    enabled:false
                                    },
                                    title: {
                                    text: ''
                                    },
                                    xAxis: {
                                    categories: lacategorie
                                    },
                                    series: [{
                                    name: 'Motivations',
                                    "color" : "#ff0000",
                                    data: lavaleur
                                    }]
                                    });
                                    });
                                </script> <script type="text/javascript">
lavaleur.push(3);
                                    lacategorie.push('Les occasions de développer des relations');
                                </script> <script type="text/javascript">
lavaleur.push(1);
                                    lacategorie.push('Le sentiment d\'estime');
                                </script> <script type="text/javascript">
lavaleur.push(4);
                                    lacategorie.push('L\'indépendance de pensée et d\'action');
                                </script> <script type="text/javascript">
lavaleur.push(3);
                                    lacategorie.push('L\'établissement de buts et d\'objectifs');
                                </script> <script type="text/javascript">
lavaleur.push(4);
                                    lacategorie.push('L\'établissement de méthodes');
                                </script> <script type="text/javascript">
lavaleur.push(2);
                                    lacategorie.push('La sécurité de l\'emploi');
                                </script> <script type="text/javascript">
lavaleur.push(3);
                                    lacategorie.push('La rétribution');
                                </script> <script type="text/javascript">
lavaleur.push(2);
                                    lacategorie.push('Le sentiment de prestige');
                                </script> <script type="text/javascript">
lavaleur.push(2);
                                    lacategorie.push('L\'autorité attachée é position');
                                </script> <script type="text/javascript">
lavaleur.push(4);
                                    lacategorie.push('Le développement et le progrés personnel');
                                </script>

                                <div class="chartt" id="graphique_motivations" style="height: 400px"></div>
                            </div><!--#right-col-->
                        </div><!--#tabs-3-->

                        <div id="tabs-4" class="clearfix tab docs">
                            <p class="tab-head">Certification</p>

                            <p>Les documents présentés sont réguliérement mis é jour par les candidats ou les opérateurs chargés de leur accompagnement. Ils sont certifiés comme exacts par leurs déposants dans un cadre contractuel avec notre équipe. Le dépét de tout document incomplet, erroné, ou faux entraéne des sanctions graduées pouvant aller jusqu'é la suspension de la publication du profil du candidat ou sa radiation de notre base de données. Merci de nous avertir si vous constatez une inexactitude ou un faux.</p>

                            <div id="left-col" class="left-col">
                                <p class="tab-head tabdocs">le CV</p>

                                <p class="tab-cell synt">Cliquez <a href="http://jobvideo.fr/wp-content/uploads/2015/03/modele-cv.pdf" target="_blank">ici</a> pour télécharger</p>

                                <p class="tab-head tabdocs">la lettre de motivation</p>

                                <p class="tab-cell synt">Cliquez <a href="http://jobvideo.fr/wp-content/uploads/2015/03/modele-lettre.pdf" target="_blank">ici</a> pour télécharger</p>

                                <p class="tab-head tabdocs">attestation(s)</p>

                                <p class="tab-cell synt">Cliquez <a href="http://jobvideo.fr/wp-content/uploads/2015/03/attestation.pdf" target="_blank">ici</a> pour télécharger</p>
                            </div><!--#left-col-->

                            <div id="right-col" class="right-col">
                                <p class="tab-head tabdocs">les diplémes</p>

                                <p class="tab-cell synt">Cliquez <a href="http://jobvideo.fr/wp-content/uploads/2015/03/diplome.pdf" target="_blank">ici</a> pour télécharger</p>

                                <p class="tab-cell synt">Cliquez <a href="http://jobvideo.fr/wp-content/uploads/2015/03/diplome.pdf" target="_blank">ici</a> pour télécharger</p>

                                <p class="tab-head tabdocs">certificat(s)</p>

                                <p class="tab-cell synt">Cliquez <a href="http://jobvideo.fr/wp-content/uploads/2015/03/certificat.pdf" target="_blank">ici</a> pour télécharger</p>
                            </div><!--#right-col-->
                        </div><!--#tabs-4-->

                        <div id="tabs-5" class="clearfix tab coord">
                            <div id="left-col" class="left-col">
                                <ul class="tab-cell caract">
                                    <li><strong>nom</strong>Durand</li>

                                    <li><strong>prénom</strong>éric</li>

                                    <li><strong>téléphones</strong>06 72 01 01 01, 02 35 01 01 01</li>

                                    <li><strong>email</strong>monadressemail@mommail.com</li>

                                    <li><strong>sexe</strong></li>

                                    <li><strong>Né(e) le</strong>31/12/1961</li>

                                    <li><strong>Département</strong>Seine-Maritime</li>

                                    <li><strong>Adresse</strong>1 rue du Front de mer - 76600 - Le Havre</li>
                                </ul>
                            </div><!--#left-col-->

                            <div id="right-col" class="right-col">
                                <ul class="tab-cell caract">
                                    <li><strong>mobilité</strong>500 km</li>

                                    <li><strong>permis de conduire</strong>Oui</li>

                                    <li><strong>moyen de transport</strong><?php $candidat_moyen_transport; ?></li>

                                    <li><strong>handicap</strong>Oui</li>

                                    <li><strong>aménagement de poste</strong>Oui</li>

                                    <li><strong>lequel ?</strong></li>

                                    <li><strong>Se définit comme</strong>dynamique et motivé</li>

                                    <li><strong>Expériences</strong>Formateur-consultantAgent d'exploitation aérien</li>
                                </ul>
                            </div><!--#right-col-->

                            <p class="tab-head">synthése rh</p>

                            <p class="tab-cell synt">Cliquez <a href="http://jobvideo.fr/wp-content/uploads/2015/03/modele-synthese-rh.pdf" target="_blank">ici</a> pour télécharger</p>
                        </div><!--#tabs-5-->
                    </div><!--#content-tab-->
                </div>
            </div>
        </article>
    </section>
	<?php //include ('include/footer.php'); ?>
 


 
<?php
	}
	?>