<?php include ('include/header.php'); ?>
<section id="featured" class="featured featured-inscription clearfix">
    <div id="image-featured" class="image-featured">
        <h3>Étape 3</h3>
        <h1>votre panier</h1>
        <p>Voici votre récapitulatif de votre abonnement</p>
    </div>
</section>
<section id="featured-log" class="featured-log clearfix wrapper">
    <div class="tab-bucket clearfix">
	    <div class="buy-abo">
	    <h3>Récapitulatif de vos achats d'abonnement</h3>
	        <ul class="tab-bucket-header clearfix">
	            <li class="tab-cell tab-55"><label for="designation">Désignation</label></li>
	            <li class="tab-cell tab-20 qte"><label for="quantity">Quantité</label></li>
	            <li class="tab-cell tab-20 cd-price"><label for="price">Prix</label></li>
	            <li class="tab-cell tab-5 delete"><label for="delete">Supprimer</label></li>
	        </ul>
	        <ul class="tab-bucket-content clearfix">
	             <li class="tab-abo">
	                 <div class="tab-cell tab-55">
	                     <h2>BASIC</h2>
	                     <div class="cd-pricing-features">
	                        <ul>
	                            <li>Accès au moteur de recherche</li>
	                            <li>Accès au profil partiel (vidéo et synthèse)</li>
	                            <li>Achat de profil candidat</li>
	                        </ul>
	                         <p>1 profil complet gratuit / an</p>
	                     </div>
	                 </div>
	                 <div class="tab-cell tab-20 qte">
	                     <a>+</a><input type="text" value="1"><a>-</a>
	                 </div>
	                  <div class="tab-cell tab-20 cd-price">
	                        <span>15€</span>
	                 </div>
	                 <div class="tab-cell tab-5 delete">
	                     <input type="reset"/>
	                 </div>
	            </li>
	            <li class="tab-abo">
	                 <div class="tab-cell tab-55">
	                     <h2>Profil complet</h2>
	                 </div>
	                 <div class="tab-cell tab-20 qte">
	                     <a>+</a><input type="text" value="4"/><a>-</a>
	                 </div>
	                  <div class="tab-cell tab-20 cd-price">
	                        <span>20€</span>
	                 </div>
	                 <div class="tab-cell tab-5 delete">
	                     <input type="reset"/>
	                 </div>
	            </li>
	        </ul>
	    </div>
<!--achat profil -->
        <div class="buy-profil">
	        <h3>Récapitulatif de vos achats de profil</h3>
	        <ul class="tab-bucket-header clearfix">
	            <li class="tab-cell tab-75"><label for="designation">Désignation</label></li>
	            <li class="tab-cell tab-20 cd-price"><label for="price">Prix</label></li>
	            <li class="tab-cell tab-5 delete"><label for="delete">Supprimer</label></li>
	        </ul>
	        <ul class="tab-bucket-content clearfix">
	             <li class="tab-abo">
	                 <div class="tab-cell tab-75">
	                    <figure id="portrait" class="portrait">
						<img src="http://jobvideo.fr/wp-content/uploads/2015/03/portrait-eric.jpg" alt="" />
						</figure>
	                     <h2>Éric</h2>
	                     <div class="cd-pricing-features">
	                        <ul>
	                            <li>Formateur-consultant</li>
	                            <li>Le Havre</li>

	                        </ul>
	                         <p>Réf. 76jbv000test</p>
	                     </div>
	                 </div>
	                  <div class="tab-cell tab-20 cd-price">
	                        <span>20€</span>
	                 </div>
	                 <div class="tab-cell tab-5 delete">
	                     <input type="reset"/>
	                 </div>
	            </li>
	        </ul>
        </div>
        <ul class="tab-bucket-total clearfix">
            <li class="tab-abo">
                <p class="tab-cell tab-75 tab-total">
                    Total h.t.
                </p>
                <p class="tab-cell tab-20 tab-total tab-total-ht">
                    29,16 € h.t.
                </p>
            </li>
            <li class="tab-abo">
                <p class="tab-cell tab-75 tab-total">
                    <strong>Total t.t.c.</strong>
                </p>
                <p class="tab-cell tab-20 tab-total tab-total-ttc">
                    <strong>35 € t.t.c.</strong>
                </p>
            </li>
        </ul>
        <ul class="paiement-choice">
            <li class="tab-abo">
                <p class="tab-cell tab-50">choix du mode de paiement</p>
            </li>
            <li class="tab-abo">
                <input id="paiement1" methode="POST" type="checkbox" value="paiement par chèque"/><label for="paiement1">paiement par chèque</label>
                <span>pour un paiement par chèque veuillez le libellé à l'ordre de RECRUTWEB<b/>et l'envoyer à RECRUT WEB - BP 20012 - 76301 Sotteville-lès-Rouen CEDEX </span>
            </li>
        	<li class="tab-abo">
                <input id="paiement2" methode="POST" type="checkbox" value="paiement par carte bleue"/><label for="paiement2">paiement par carte bleue</label>
            </li>

        </ul>
        <div class="tab-bucket-commande">
            <input methode="POST" type="submit" value="régler mon abonnement">
        </div>
    </div>
</section>
<?php
include ('include/footer.php');