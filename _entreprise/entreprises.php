﻿ <?php

  	$id_entreprise = $_SESSION['id_entreprise'] ;

   $sql_entreprise = "select * from entreprise where 	entreprise_id='$id_entreprise'";
	$res_entreprise = $connexion->query($sql_entreprise);
	$donnees_entreprise  = $res_entreprise->fetch();
   $nom = $donnees_entreprise['entreprise_raison_social'] ;
  $entreprise_domaine_activite = $donnees_entreprise['entreprise_domaine_activite'] ;
  $sirete = $donnees_entreprise['entreprise_num_siret'] ;
  $entreprise_date_modif = $donnees_entreprise['entreprise_date_modif'] ;
  $entreprise_photo = $donnees_entreprise['entreprise_photo'] ;
  $entreprise_video_url = $donnees_entreprise['entreprise_video_url'] ;
  $entreprise_adresse = $donnees_entreprise['entreprise_adresse'] ;
  $entreprise_code_postale = $donnees_entreprise['entreprise_code_postale'] ;
  $entreprise_vile   = $donnees_entreprise['entreprise_vile'] ;
  $entreprise_tel = $donnees_entreprise['entreprise_tel'] ;
  $entreprise_ref = $donnees_entreprise['entreprise_ref'] ;
  $entreprise_description = utf8_encode($donnees_entreprise['entreprise_description']) ;
	$entreprise_email = $donnees_entreprise['entreprise_email'] ;
if($entreprise_photo=="") $entreprise_photo = "vide.png";

 $sql_consulter	= "SELECT * FROM  consulter, candidat  WHERE  id_entreprise  ='$id_entreprise' and id_candidat = candidat_id ";
 
 	$res_consulter = $connexion->query($sql_consulter);
   $nb_consult = $res_consulter->rowCount() ; 

	 $sql_acheter	= "SELECT * FROM  acheter,candidat  WHERE  id_entreprise  ='$id_entreprise'and archiver ='N' and id_candidat = candidat_id and valide='Y'";
	 $res_acheter = $connexion->query($sql_acheter);
   $nb_acht = $res_acheter->rowCount() ; 
 

?>
<section id="featured" class="featured featured-inscription clearfix">
   <article id="intro-profil" role="section" class="intro-profil clearfix">
    <div id="image-featured" class="image-featured">
        <h3>bonjour, <?php echo $nom; ?></h3>
			<h1>Bienvenue dans votre espace</h1>
			<p>Ici vous pouvez faire vos recherches de cv multimédia, acheter des profils, noter vos candidats, consulter votre historique.</p>
    </div>
   </article>
</section>
<section id="profil" role="section" class="featured-profil-entreprise clearfix">
	<div id="cv" class="cv clearfix">
		<div id="cv-tabs" class="cv-tabs">
			<div class="cv-tabs-inner clearfix">
				<ul id="tabs" class="tabs clearfix">

					<li><a class="tab1 activate" id="firstonglet" href="./?p=profil&session=<?php echo $session ?>#tabs-1" title="Voir votre profil">Votre profil</a></li>
					<li><a class="tab2" id="#tabs-2" href="./?p=profils_consulte&session=<?php echo $session ?>#tabs-2"  title="Historique de vos consultations"><span class="nbre_ex"><?php echo $nb_consult?></span><span class="txtNbre">Profil(s) consulté(s)</span></a></li>
					<li><a class="tab3" id="#tabs-3" href="./?p=profils_achete&session=<?php echo $session ?>#tabs-3"  title="Historique de vos achats"><span class="nbre_ex"><?php echo $nb_acht?></span><span class="txtNbre">Profil(s) acheté(s)</span></a></li>
					<li><a class="tab4" id="#tabs-4" href="./?p=editer_profil&session=<?php echo $session ?>#tabs-4"  title="Éditer votre profil">Éditer son profil</a></li>
					<li><a class="tab5" id="#tabs-5" href="./?p=rechercher&session=<?php echo $session ?>#tabs-5"  title="Faire une recherche cv multimédia">Rechercher un candidat</a></li>
					<li><a class="tab6" id="#tabs-6" href="./?p=abonner&session=<?php echo $session ?>#tabs-6"  title="S'abonner ou acheter">S'abonner acheter un cv candidat</a></li>
				</ul>
			</div>
			<div id="content-tab" class="content-tab">
				
			<div id="tabs-1" class="clearfix tab pres">
					<article class="cv-content-inner clearfix">
						<figure id="portrait" class="portrait portrait-entreprise">
							<img src="./common/Images/entreprise/<?php echo $entreprise_photo; ?>" alt="logo-entreprise"/>
						</figure>
						<div class="cv-text">
							<h2> <?php echo $nom; ?></h2>
							<h2 class="skills"><?php echo $entreprise_domaine_activite; ?></h2>
							<h3><?php echo $entreprise_adresse; ?>,<?php echo $entreprise_code_postale; ?> - <?php echo $entreprise_vile; ?></h3>
							<h3 class="tel">Tél : <?php echo $entreprise_tel; ?></h3><span class="seperate">|</span><h3 class="siret">Siret : <?php echo $sirete; ?></h3>
							<ul id
								="ref" class="ref">
								<li>Dernière mise à jour :  <?php echo $entreprise_date_modif; ?></li>
								<li>Réf. <?php echo $entreprise_ref; ?></li>
							</ul>
						</div>
						<div id="left-col" class="left-col">
								<p class="tab-head">vidéo</p>
									<div class="embed-container"><iframe width="200" height="113" src="<?php echo $entreprise_video_url; ?>" frameborder="0" allowfullscreen></iframe></div>
						</div>
						<!--#left-col-->
						<div id="right-col" class="right-col">
							<div class="head">
								<p class="title-tab-head tab-head">Les plus de l’entreprise</p>
									<ul class="tab-cell">
											<li class="exp"><?php echo $entreprise_description; ?>.</li>
 									</ul>
							</div>
						</div>
						<!--#right-col-->
					</article>
				</div>
				
			</div>
		</div>
	</div>
</section>

