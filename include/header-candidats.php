<?php include('../include/header.php');?>
<body id="body" class="page cv-candidat">
	<div class="overlay" id="overlay"></div>
	<main id="main" role="main">
		<div class="btn_up"></div>
		<header id="header" class="header clearfix">
			<nav id="nav-widget" class="nav-widget clearfix">
				<div class="wrapper">
					<ul class="breadcrumbs">
						<li><a class="home-icon" href="http://jobvideo.fr">Accueil</a></li>
						<li class="separator"> > </li>
						<li>Vous êtes ici : prévoir ici un breadcrumb</li>
					</ul>


				</div>

			</nav>
			<nav id="nav-header" class="nav-header wrapper clearfix">
				<div id="nav-inner" class="navInner">
					<a href="#" id="nav-Responsive" class="navResponsive" title="ouvrir la navigation"><span class="menuBurger">ouvrir la navigation</span></a>
					<a id="logo" class="logo" href="http://jobvideo.fr/" title="retour à l'accueil">jobvideo.fr</a>
				</div>
				<div id="nav-top" class="menu-entreprises-container navTop">
					<ul id="menu-entreprises" class="menu-header clearfix">
						<li id="menu-item-123" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-123">
							<a href="http://jobvideo.fr/deontologie/">Déontologie</a>
						</li>
						<li id="menu-item-124" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-124">
							<a href="http://jobvideo.fr/historique/">Historique</a>
						</li>
						<li id="menu-item-128" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-128">
							<a title="pour nous contacter" href="http://jobvideo.fr/#featured-contact">contactez-nous</a>
						</li>
						<li id="menu-item-124" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-124">
							<a href="index.php?session=<?php echo $idses?>&ferms=1">Déconnexion</a>
						</li>
					</ul>
				</div>
			</nav>
		</header>