<?php
$lang="fr";
/****************************************************************************
                      Supprime une liste d'actualit�s
****************************************************************************/
function question_type_supprimer($tab) {
global $lang;
  if (count($tab)==0)         // Param�tre vide ?
    return;
	
  $str = implode(", ", $tab);
   
  //---> Supprimer les images
     
  
   //---> Supprimer la liste des produits de de ces question_type
  include "question.php";
  $sql = "SELECT  DISTINCT question_id
          FROM   question
		  WHERE  question_type_id IN ($str)";

  $res = $connexion->query($sql);
   
   $id  = array(); 
  while ($row  = $res->fetch())
  {
    $id[] = $row['question_id'];
  } //FTQ
  question_supprimer($id);
  
  //---> Suppresion effective de la base de donn�es
  $sql = "DELETE FROM question_type
          WHERE question_type_id IN ($str)";
  $res = $connexion->prepare($sql);
	$res->execute();
} //Fin question_type_supprimer
/**************************************************************************************************
                       Modifier la propri�t� visible d'un ensemble de question_type
**************************************************************************************************/
function question_type_visible($tab, $tab_id)
{
  $id  = implode(", ", $tab_id);
  $sql = "UPDATE question_type
          SET question_type_visible = 'N'
          WHERE question_type_id IN ($id)";
  $res = $connexion->prepare($sql);
	$res->execute();
  if (count($tab)>0)
  {
    $str = implode(", ", $tab);
    $sql = "UPDATE question_type
            SET   question_type_visible = 'Y'
            WHERE question_type_id IN ($str)";
    $res = $connexion->prepare($sql);
	$res->execute();     
  } //Fsi			
} //Fin question_type_visible
/**************************************************************************************************
                       Modifier la propri�t� une d'un ensemble de question_type
**************************************************************************************************/
function question_type_une($tab, $tab_id)
{
  $id  = implode(", ", $tab_id);
  $sql = "UPDATE question_type
          SET   question_type_une = 'N'
          WHERE question_type_id IN ($id)";
  $res = $connexion->prepare($sql);
	$res->execute();
  if (count($tab)>0)
  {
    $str = implode(", ", $tab);
    $sql = "UPDATE question_type
            SET   question_type_une = 'Y'
            WHERE question_type_id IN ($str)";
    $res = $connexion->prepare($sql);
	$res->execute();     
  } //Fsi			
} //Fin question_type_une

////////////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////////////
function question_type_deplacer($id,$sens)
{
  executer("LOCK TABLES question_type WRITE",__FILE__,__LINE__);
  if ($sens=="bas") 
  {
	$sql = "SELECT MIN(question_type_position) as MAX
			FROM   question_type
			WHERE  question_type_position > $id";
 }
  elseif($sens=="haut")
 {
	$sql = "SELECT MAX(question_type_position) as MAX 
			FROM   question_type
			WHERE  question_type_position < $id";
  }
  $res = $connexion->query($sql);
  $row  = $res->fetch();
  $id2 = $row['MAX'];
  
   
  if ($id2!="")
  {
	$sql = "UPDATE question_type
	        SET    question_type_position         = 0
			WHERE  question_type_position          = $id2";
	$res = $connexion->prepare($sql);
	$res->execute();
	$sql = "UPDATE question_type
	        SET    question_type_position          = $id2
			WHERE  question_type_position          = $id";
	$res = $connexion->prepare($sql);
	$res->execute();
	$sql = "UPDATE question_type
	        SET    question_type_position          = $id
			WHERE  question_type_position          = 0"  ;
	$res = $connexion->prepare($sql);
	$res->execute();
  } //Fsi
  executer("UNLOCK TABLES",__FILE__,__LINE__);
} //Fin clients_deplacer_bas

/****************************************************************************
                   Mettre � jour le fichier de la photo
				                  et
					Supprimer le fichier pr�c�dent
****************************************************************************/
function question_type_photo_update($question_type_id, $filename, $ext)
{
global $lang;
  $sql = " SELECT question_type_photo FROM question_type WHERE question_type_id = '$question_type_id'";
  $res = $connexion->query($sql);
  $row  = $res->fetch();
  $question_type_photo = stripcslashes($row['question_type_photo']);
   
  if($question_type_photo != 'img_vide.gif')
	{@unlink("../common/Images/question_type/$question_type_photo");}
   $fn  = "question_type($question_type_id).$ext";
  copy($filename, "../common/Images/question_type/$fn");
  $sql = "UPDATE question_type
	      SET    question_type_photo = ''
		  WHERE  question_type_id    = " . $_GET['question_type_id'];
  $res = $connexion->prepare($sql);
	$res->execute();
} //Fin question_type_photo_update
//****************************************************************************/



/****************************************************************************
   			        D�finir l'objet de pagination
****************************************************************************/
function question_type_pagination_object()
{
 $p   = new CPagination("question_type","question_type_visible = 'Y'",5,"question_type_position","ASC");
 return $p;
} //Fin question_type_pagination_object
?>