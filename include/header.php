<!DOCTYPE html>
<!--[if IE 7]><html class="ie ie7" lang="fr-FR"><![endif]-->
<!--[if IE 8]><html class="ie ie8" lang="fr-FR"><![endif]-->
<!--[if !(IE 7) | !(IE 8)  ]><!-->
<!--<![endif]-->
<html lang="fr-FR">

<head>
	<meta charset="UTF-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="IE=edge, chrome=1">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>cvweb | jobvideo.fr &#124; Le CV vidéo &#124; abonnements</title>
	<link rel="profile" href="http://gmpg.org/xfn/11" />
	<link rel="shortcut icon" href="/images/favicon.ico" type="image/x-icon">
	<link rel="icon" href="/images/favicon.ico" type="image/x-icon">
	<link rel="stylesheet" type="text/css" media="all" href="../css/styles.css" />
	<link rel="stylesheet" type="text/css" media="all" href= "../css/style_admin.css">
	<link href='http://fonts.googleapis.com/css?family=Roboto:400,700' rel='stylesheet' type='text/css'>
	<link href='http://fonts.googleapis.com/css?family=Titillium+Web:400,700' rel='stylesheet' type='text/css'>
	<link href='http://fonts.googleapis.com/css?family=Nunito:700' rel='stylesheet' type='text/css'>
	<link rel="pingback" href="http://jobvideo.fr/xmlrpc.php" />
	<script language="javascript" type="text/javascript" src="../js/scripts.js"></script>
	<!--[if gte IE 9]><style type="text/css">.gradient {filter: none;}</style><![endif]-->
	<!--[if lt IE 9]><script src="scripts/html5shiv.js"></script><![endif]-->
	<!--[if IE]><link rel="stylesheet" type="text/css" href="css/ie.css" media="screen"><![endif]-->
	<!--[if lt IE 9]><style>.content{height: auto; margin: 0;}	.content div {position: relative;}</style><![endif]-->

</head>