<?php
$lang="fr";
/****************************************************************************
                      Supprime une liste d'actualit�s
****************************************************************************/
function partie_synthese_supprimer($tab) {
global $lang;
  if (count($tab)==0)         // Param�tre vide ?
    return;
	
  $str = implode(", ", $tab);
   
   
   //---> Supprimer la liste des produits de de ces partie_synthese
  include "question_type.php";
  $sql = "SELECT  DISTINCT question_type_id
          FROM   question_type
		  WHERE  question_type_partie_synthese_id IN ($str)";
    $res = $connexion->query($sql);
	 
	$id  = array(); 
  while ($row =$res->fetch())
  {
    $id[] = $row['question_type_id'];
  } //FTQ
  question_type_supprimer($id);
  
  //---> Suppresion effective de la base de donn�es
  $sql = "DELETE FROM partie_synthese
          WHERE partie_synthese_id IN ($str)";
  $res = $connexion->prepare($sql);
	$res->execute();
} //Fin partie_synthese_supprimer
/**************************************************************************************************
                       Modifier la propri�t� visible d'un ensemble de partie_synthese
**************************************************************************************************/
function partie_synthese_visible($tab, $tab_id)
{
  $id  = implode(", ", $tab_id);
  $sql = "UPDATE partie_synthese
          SET partie_synthese_visible = 'N'
          WHERE partie_synthese_id IN ($id)";
  $res = $connexion->prepare($sql);$res->execute();
  if (count($tab)>0)
  {
    $str = implode(", ", $tab);
    $sql = "UPDATE partie_synthese
            SET   partie_synthese_visible = 'Y'
            WHERE partie_synthese_id IN ($str)";
    $res = $connexion->prepare($sql);$res->execute();     
  } //Fsi			
} //Fin partie_synthese_visible
/**************************************************************************************************
                       Modifier la propri�t� une d'un ensemble de partie_synthese
**************************************************************************************************/
function partie_synthese_une($tab, $tab_id)
{
  $id  = implode(", ", $tab_id);
  $sql = "UPDATE partie_synthese
          SET   partie_synthese_une = 'N'
          WHERE partie_synthese_id IN ($id)";
  $res = $connexion->prepare($sql);$res->execute(); 
  if (count($tab)>0)
  {
    $str = implode(", ", $tab);
    $sql = "UPDATE partie_synthese
            SET   partie_synthese_une = 'Y'
            WHERE partie_synthese_id IN ($str)";
    $res = $connexion->prepare($sql);$res->execute();     
  } //Fsi			
} //Fin partie_synthese_une

////////////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////////////
function partie_synthese_deplacer($id,$sens)
{
  executer("LOCK TABLES partie_synthese WRITE",__FILE__,__LINE__);
  if ($sens=="bas") 
  {
	$sql = "SELECT MIN(partie_synthese_position) as MAX 
			FROM   partie_synthese
			WHERE  partie_synthese_position > $id";
 }
  elseif($sens=="haut")
 {
	$sql = "SELECT MAX(partie_synthese_position)as MAX 
			FROM   partie_synthese
			WHERE  partie_synthese_position < $id";
  }
	$res = $connexion->query($sql);
	$row  = $res->fetch();
	$id2 =  $row['MAX'] ;
   if ($id2!="")
  {
	$sql = "UPDATE partie_synthese
	        SET    partie_synthese_position         = 0
			WHERE  partie_synthese_position          = $id2";
	$res = $connexion->prepare($sql);$res->execute();
	$sql = "UPDATE partie_synthese
	        SET    partie_synthese_position          = $id2
			WHERE  partie_synthese_position          = $id";
	$res = $connexion->prepare($sql);$res->execute();
	$sql = "UPDATE partie_synthese
	        SET    partie_synthese_position          = $id
			WHERE  partie_synthese_position          = 0"  ;
	$res = $connexion->prepare($sql);$res->execute();
  } //Fsi
  executer("UNLOCK TABLES",__FILE__,__LINE__);
} //Fin clients_deplacer_bas

/****************************************************************************
                   Mettre � jour le fichier de la photo
				                  et
					Supprimer le fichier pr�c�dent
****************************************************************************/
function partie_synthese_photo_update($partie_synthese_id, $filename, $ext)
{
global $lang;
  $sql = " SELECT partie_synthese_photo FROM partie_synthese WHERE partie_synthese_id = '$partie_synthese_id'";
   $res = $connexion->query($sql);
	$row  = $res->fetch();
	$partie_synthese_photo =  stripcslashes($row['partie_synthese_photo']) ;
	
   
  if($partie_synthese_photo != 'img_vide.gif')
	{@unlink("../common/Images/partie_synthese/$partie_synthese_photo");}
   $fn  = "partie_synthese($partie_synthese_id).$ext";
  copy($filename, "../common/Images/partie_synthese/$fn");
  $sql = "UPDATE partie_synthese
	      SET    partie_synthese_photo = ''
		  WHERE  partie_synthese_id    = " . $_GET['partie_synthese_id'];
  $res = $connexion->prepare($sql);$res->execute();
} //Fin partie_synthese_photo_update
//****************************************************************************/



/****************************************************************************
   			        D�finir l'objet de pagination
****************************************************************************/
function partie_synthese_pagination_object()
{
 $p   = new CPagination("partie_synthese","partie_synthese_visible = 'Y'",5,"partie_synthese_position","ASC");
 return $p;
} //Fin partie_synthese_pagination_object
?>