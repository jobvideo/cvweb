<?php
$lang="fr";
/****************************************************************************
                      Supprime une liste d'actualit�s
****************************************************************************/
function reponse_question_supprimer($tab) {
global $lang;
  if (count($tab)==0)         // Param�tre vide ?
    return;
	
  $str = implode(", ", $tab);
   
  //---> Supprimer les images
     
  
   //---> Supprimer la liste des produits de de ces reponse_question
  include "reponse_question.php";
  $sql = "SELECT  DISTINCT reponse_question_id
          FROM   reponse_question
		  WHERE  reponse_question_id IN ($str)";
		  
  $res = $connexion->query($sql);  
   $id  = array(); 
  while ($row  = $res->fetch())
  {
    $id[] = $row['reponse_question_id'];
  } //FTQ
  reponse_question_supprimer($id);
  
  //---> Suppresion effective de la base de donn�es
  $sql = "DELETE FROM reponse_question
          WHERE reponse_question_id IN ($str)";
   $res = $connexion->prepare($sql);
	$res->execute();
   
} //Fin reponse_question_supprimer
/**************************************************************************************************
                       Modifier la propri�t� visible d'un ensemble de reponse_question
**************************************************************************************************/
function reponse_question_visible($tab, $tab_id)
{
  $id  = implode(", ", $tab_id);
  $sql = "UPDATE reponse_question
          SET reponse_question_visible = 'N'
          WHERE reponse_question_id IN ($id)";
	$res = $connexion->prepare($sql);
	$res->execute();
   if (count($tab)>0)
  {
    $str = implode(", ", $tab);
    $sql = "UPDATE reponse_question
            SET   reponse_question_visible = 'Y'
            WHERE reponse_question_id IN ($str)";
    $res = $connexion->prepare($sql);
	$res->execute();   
  } //Fsi			
} //Fin reponse_question_visible
/**************************************************************************************************
                       Modifier la propri�t� une d'un ensemble de reponse_question
**************************************************************************************************/
function reponse_question_une($tab, $tab_id)
{
  $id  = implode(", ", $tab_id);
  $sql = "UPDATE reponse_question
          SET   reponse_question_une = 'N'
          WHERE reponse_question_id IN ($id)";
   $res = $connexion->prepare($sql);
	$res->execute();
  if (count($tab)>0)
  {
    $str = implode(", ", $tab);
    $sql = "UPDATE reponse_question
            SET   reponse_question_une = 'Y'
            WHERE reponse_question_id IN ($str)";
     $res = $connexion->prepare($sql);
	$res->execute();
  } //Fsi			
} //Fin reponse_question_une

////////////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////////////
function reponse_question_deplacer($id,$sens)
{
  executer("LOCK TABLES reponse_question WRITE",__FILE__,__LINE__);
  if ($sens=="bas") 
  {
	$sql = "SELECT MIN(reponse_question_position) as MAX
			FROM   reponse_question
			WHERE  reponse_question_position > $id";
 }
  elseif($sens=="haut")
 {
	$sql = "SELECT MAX(reponse_question_position) as MAX
			FROM   reponse_question
			WHERE  reponse_question_position < $id";
  }
  $res = $connexion->query($sql);
  $row  = $res->fetch();
  $id2 = $row['MAX'];
   
  if ($id2!="")
  {
	$sql = "UPDATE reponse_question
	        SET    reponse_question_position         = 0
			WHERE  reponse_question_position          = $id2";
	$res = $connexion->prepare($sql);
	$res->execute();
	$sql = "UPDATE reponse_question
	        SET    reponse_question_position          = $id2
			WHERE  reponse_question_position          = $id";
	$res = $connexion->prepare($sql);
	$res->execute();
	$sql = "UPDATE reponse_question
	        SET    reponse_question_position          = $id
			WHERE  reponse_question_position          = 0"  ;
	$res = $connexion->prepare($sql);
	$res->execute();
  } //Fsi
  executer("UNLOCK TABLES",__FILE__,__LINE__);
} //Fin clients_deplacer_bas

/****************************************************************************
                   Mettre � jour le fichier de la photo
				                  et
					Supprimer le fichier pr�c�dent
****************************************************************************/
function reponse_question_photo_update($reponse_question_id, $filename, $ext)
{
global $lang;
  $sql = " SELECT reponse_question_photo FROM reponse_question WHERE reponse_question_id = '$reponse_question_id'";
  $res = $connexion->query($sql);
  $row  = $res->fetch();
  $reponse_question_photo = stripcslashes($row['reponse_question_photo']);
   
  if($reponse_question_photo != 'img_vide.gif')
	{@unlink("../common/Images/reponse_question/$reponse_question_photo");}
   $fn  = "reponse_question($reponse_question_id).$ext";
  copy($filename, "../common/Images/reponse_question/$fn");
  $sql = "UPDATE reponse_question
	      SET    reponse_question_photo = ''
		  WHERE  reponse_question_id    = " . $_GET['reponse_question_id'];
   $res = $connexion->prepare($sql);
	$res->execute();
} //Fin reponse_question_photo_update
//****************************************************************************/



/****************************************************************************
   			        D�finir l'objet de pagination
****************************************************************************/
function reponse_question_pagination_object()
{
 $p   = new CPagination("reponse_question","reponse_question_visible = 'Y'",5,"reponse_question_position","ASC");
 return $p;
} //Fin reponse_question_pagination_object
?>