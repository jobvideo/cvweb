<?php
$lang="fr";
/****************************************************************************
                      Supprime une liste d'actualit�s
****************************************************************************/
function question_supprimer($tab) {
global $lang;
  if (count($tab)==0)         // Param�tre vide ?
    return;
	
  $str = implode(", ", $tab);
   
  //---> Supprimer les images
     
  
   //---> Supprimer la liste des produits de de ces question
  include "question.php";
  $sql = "SELECT  DISTINCT question_id
          FROM   question
		  WHERE  question_id IN ($str)";
  $res = $connexion->query($sql);
   
    $id  = array(); 
  while ($row  = $res->fetch())
  {
    $id[] = $row['question_id'];
  } //FTQ
  question_supprimer($id);
  
  //---> Suppresion effective de la base de donn�es
  $sql = "DELETE FROM question
          WHERE question_id IN ($str)";
  $res = $connexion->prepare($sql);
	$res->execute();
} //Fin question_supprimer
/**************************************************************************************************
                       Modifier la propri�t� visible d'un ensemble de question
**************************************************************************************************/
function question_visible($tab, $tab_id)
{
  $id  = implode(", ", $tab_id);
  $sql = "UPDATE question
          SET question_visible = 'N'
          WHERE question_id IN ($id)";
  $res = $connexion->prepare($sql);
	$res->execute();
  if (count($tab)>0)
  {
    $str = implode(", ", $tab);
    $sql = "UPDATE question
            SET   question_visible = 'Y'
            WHERE question_id IN ($str)";
    $res = $connexion->prepare($sql);
	$res->execute();  
  } //Fsi			
} //Fin question_visible
/**************************************************************************************************
                       Modifier la propri�t� une d'un ensemble de question
**************************************************************************************************/
function question_une($tab, $tab_id)
{
  $id  = implode(", ", $tab_id);
  $sql = "UPDATE question
          SET   question_une = 'N'
          WHERE question_id IN ($id)";
  $res = $connexion->prepare($sql);
	$res->execute();
  if (count($tab)>0)
  {
    $str = implode(", ", $tab);
    $sql = "UPDATE question
            SET   question_une = 'Y'
            WHERE question_id IN ($str)";
    $res = $connexion->prepare($sql);
	$res->execute();   
  } //Fsi			
} //Fin question_une

////////////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////////////
function question_deplacer($id,$sens)
{
  executer("LOCK TABLES question WRITE",__FILE__,__LINE__);
  if ($sens=="bas") 
  {
	$sql = "SELECT MIN(question_position) as MAX
			FROM   question
			WHERE  question_position > $id";
 }
  elseif($sens=="haut")
 {
	$sql = "SELECT MAX(question_position) as MAX
			FROM   question
			WHERE  question_position < $id";
  }
  $res = $connexion->query($sql);
  $row  = $res->fetch();
  $id2 = $row['MAX'];
  
  if ($id2!="")
  {
	$sql = "UPDATE question
	        SET    question_position         = 0
			WHERE  question_position          = $id2";
	$res = $connexion->prepare($sql);
	$res->execute();
	$sql = "UPDATE question
	        SET    question_position          = $id2
			WHERE  question_position          = $id";
	$res = $connexion->prepare($sql);
	$res->execute();
	$sql = "UPDATE question
	        SET    question_position          = $id
			WHERE  question_position          = 0"  ;
	$res = $connexion->prepare($sql);
	$res->execute();		
   } //Fsi
  executer("UNLOCK TABLES",__FILE__,__LINE__);
} //Fin clients_deplacer_bas

/****************************************************************************
                   Mettre � jour le fichier de la photo
				                  et
					Supprimer le fichier pr�c�dent
****************************************************************************/
function question_photo_update($question_id, $filename, $ext)
{
global $lang;
  $sql = " SELECT question_photo FROM question WHERE question_id = '$question_id'";
  $res = $connexion->query($sql);
  $row  = $res->fetch();
  $question_photo = stripcslashes($row['question_photo']);
    
  if($question_photo != 'img_vide.gif')
	{@unlink("../common/Images/question/$question_photo");}
   $fn  = "question($question_id).$ext";
  copy($filename, "../common/Images/question/$fn");
  $sql = "UPDATE question
	      SET    question_photo = ''
		  WHERE  question_id    = " . $_GET['question_id'];
  $res = $connexion->prepare($sql);
	$res->execute();
} //Fin question_photo_update
//****************************************************************************/



/****************************************************************************
   			        D�finir l'objet de pagination
****************************************************************************/
function question_pagination_object()
{
 $p   = new CPagination("question","question_visible = 'Y'",5,"question_position","ASC");
 return $p;
} //Fin question_pagination_object
?>