<?php
session_start();
setcookie("sid", 'Y');
include_once ("./include/connexion.php");
include_once ("./include/fonctions.php");
include_once ("./include/parametres.php");

$status = "ok";  // peut prendre : no, yes, inactif
$lang = "fr";

if ((isset($_POST['login_pass'])) && (isset($_POST['user_login']))) {
    $pw = lecture($_POST['login_pass']);
    $user = lecture($_POST['user_login']);
    $sql = "SELECT *
	         FROM  admin_user
			 WHERE admin_user_nom      = '$user' AND
			       admin_user_password = '" . md5($pw) . "'";
    $res = $connexion->query($sql);
    $row = @$res->fetch();
    if ($row == FALSE) { //---> Login invalide
        $status = "no";
    } else { //---> Login correct
        if ($row['admin_user_actif'] == 'N') { //---> Utilisateurs désactivés
            unset($_COOKIE["sid"]);
            $status = "inactif";
        } else { //--> Ok !
            $date = time();
            $iduser = $row['admin_user_id'];
            $role = $row['admin_user_role'];
            $sql = "select operateur_id from operateur where admin_user_id = '$iduser'";
            $res = $connexion->query($sql);
            $row = $res->fetch();
            $operateur_id = $row['operateur_id'];
            $_SESSION['operateur_id'] = $operateur_id;
            $_SESSION['iduser'] = $iduser;
            $_SESSION['nom'] = $user;
            $_SESSION['role'] = $role;

			$sql = "select entreprise_id from entreprise where admin_user_id = '$iduser'";
            $res = $connexion->query($sql);
            $row = $res->fetch();
		   $_SESSION['id_entreprise'] = $row['entreprise_id'] ;


            $sql = "INSERT INTO session_admin
		           SET user_id      = '$iduser' ,
				       dat_ouv_ses  = '$date',
					   dat_ferm_ses = '$date'   ";
            $res = $connexion->query($sql);
            $session_id = $connexion->lastInsertId();
            $idses = md5($session_id);
            $desactive = 'yes';
            ?>
            <script language='JavaScript' type="text/javascript">

                document.location.href = './_<?php echo $role; ?>/index.php?session=<?php echo $idses; ?>&lang=<?php echo $lang ?>';

            </script>
            <?php
            exit();
        } //Fsi
    } //Fsi
} //Fsi
?>
<!DOCTYPE html>
<!--[if IE 7]><html class="ie ie7" lang="fr-FR"><![endif]-->
<!--[if IE 8]><html class="ie ie8" lang="fr-FR"><![endif]-->
<!--[if !(IE 7) | !(IE 8)  ]><!-->
<html lang="fr-FR">
    <!--<![endif]-->
    <head>
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="IE=edge, chrome=1">
        <title>Application | jobvideo.fr</title>
        <link rel="profile" href="http://gmpg.org/xfn/11" />
        <link rel="shortcut icon" href="./images/favicon.ico" type="image/x-icon">
        <link rel="icon" href="./images/favicon.ico" type="image/x-icon">
        <link rel="stylesheet" type="text/css" media="all" href="./css/styles.css" />
        <link href='http://fonts.googleapis.com/css?family=Roboto:400,700' rel='stylesheet' type='text/css'>
        <link href='http://fonts.googleapis.com/css?family=Titillium+Web:400,700' rel='stylesheet' type='text/css'>
        <link href='http://fonts.googleapis.com/css?family=Nunito:700' rel='stylesheet' type='text/css'>
        <link rel="pingback" href="http://jobvideo.fr/xmlrpc.php" />
        <!--[if gte IE 9]><style type="text/css">.gradient {filter: none;}</style><![endif]-->
        <!--[if lt IE 9]><script src="scripts/html5shiv.js"></script><![endif]-->
        <!--[if IE]><link rel="stylesheet" type="text/css" href="css/ie.css" media="screen"><![endif]-->
        <!--[if lt IE 9]><style>.content{height: auto; margin: 0;}	.content div {position: relative;}</style><![endif]-->
    </head>
    <body id="body" class="page page-login page-child parent-pageid-5 page-template page-template-PageTemplate page-template-page-tarif page-template-PageTemplatepage-tarif-php logged-in page-abonnements">
        <div class="overlay" id="overlay"></div>
        <main id="main" role="main">
            <div class="btn_up"></div>
            <header id="header" class="header clearfix">
                <nav id="nav-widget" class="nav-widget clearfix">
                    <div class="wrapper">
                        <ul class="breadcrumbs">
                            <li><a class="home-icon" href="http://jobvideo.fr">Accueil</a></li>
                            <li class="separator"> > </li>
                            <li>Vous etes ici : Application Job Vidéo</li>
                        </ul>
                        <div id="content-nav-widget" class="clearfix">
                        </div>
                    </div>
                </nav>
            </header>


            <section id="featured" class="featured clearfix">
                <div id="image-featured" class="image-featured">
                    <h1>Jobvidéo</h1>
                    <u>
                        <li> Candidats</li>
                        <li> Entreprises</li>
                        <li> Operateurs</li>
                    </u>
                </div>
            </section>
            <section id="featured-log" class="featured-log clearfix wrapper">
                <div id="log-content" class="log-content">
                    <h2>Vous etes déjà abonné ? Connectez-vous ! </h2>

                    <form name="loginform" id="loginform" action="#" method="post">

                        <p class="login-user_loginname">
                            <label for="user_login">Identifiant</label>
                            <input type="text" name="user_login" id="user_login" class="input" value="" size="20" />
                        </p>
                        <p class="login-password">
                            <label for="login_pass">Mot de passe</label>
                            <input type="password" name="login_pass" id="login_pass" class="input" value="" size="20" />
                        </p>

                        <p class="login-remember">
                            <label>
                                <input name="rememberme" type="checkbox" id="rememberme" value="forever" /> Se souvenir de moi
                            </label>
                        </p>
                        <p class="login-submit">
                            <input type="submit" name="wp-submit" id="wp-submit" class="wp-submit button-primary" value="Se connecter" />

                        </p>

                    </form>
                </div>
            </section>
<!--
            <section id="featured-inscription" class="featured-inscription clearfix wrapper">
                <h2 class="log-inscription">Vous souhaitez vous abonner ? Faites-le en 3 étapes <a href="inscriptions.php">ici</a>

                </h2>

            </section>
-->
        </main><!--#main-->
        <footer id="footer">

            <div class="nav-footer-second wrapper clearfix">
                <p class="bdd">Base de données coopérative RH</p>
                <a class="scop" href="http://www.les-scop.coop/sites/fr/">Société à statut Scop</a>
            </div>
        </footer>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
        <script type="text/javascript" src="./scripts/functions.js"></script>
        <script type="text/javascript" src="./scripts/tabs.js"></script>
        <script type="text/javascript" src="./scripts/velocity.min.js"></script>
        <script type="text/javascript" src="./scripts/main.js"></script>
        <script type="text/javascript" src="./scripts/modernizr.js"></script>
        <script type="text/javascript" src="./scripts/respond.js"></script>

        <!--JAVASCRIPT-->

        <!--INITIATION PLUG-IN-->
        <script type="text/javascript"src="scripts/init.js"></script>
        <!--PLUG-IN SLIDER-->
        <script type="text/javascript"src="scripts/responsiveslides.min.js"></script>
        <!--PLUG-IN RESPONSIVE-->


    </body>
</html>