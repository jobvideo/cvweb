$(function() {
    //SUPPRIMER <div class="textwidget">
    $('.textwidget').replaceWith($('.textwidget').contents());

    //SCROLL
    $('#menu-item-34 a').click(function(e) { // Au clic sur un élément
        var page = $(this).attr('href'); // Page cible
        var speed = 1500; // Durée de l'animation (en ms)
        $('html, body').animate({
            scrollTop: $(page).offset().top
        }, speed); // Go
        return false;
        e.preventDefault();
    });
    $('#menu-item-223 a').click(function(e) { // Au clic sur un élément
        var page = $(this).attr('href'); // Page cible
        var speed = 1500; // Durée de l'animation (en ms)
        $('html, body').animate({
            scrollTop: $(page).offset().top
        }, speed); // Go
        return false;
        e.preventDefault();
    });

    //BUTTON UP
    $('.btn_up').click(function(e) {
        $('html,body').animate({
            scrollTop: 0
        }, 'slow');
        e.preventDefault();
    });

    $(window).scroll(function() {
        if ($(window).scrollTop() < 300) {
            $('.btn_up').fadeOut();
        } else {
            $('.btn_up').fadeIn();
        }
    });
	//ACCORDEON
/*
	$('.formulaire').hide();
	$('.btn-accordeon').click(function(e){
		$(this).toggleClass('iconOn');
		$('.formulaire').slideToggle();
		e.preventDefault();
	});
*/
	
	
    //ONGLETS
    var fade = function(id, s, e) {
        s.tabs.removeClass(s.activate);
        s.tab(id).addClass(s.activate);
        s.items.fadeOut();
        s.item(id).fadeIn();
        return false;
        e.preventDefault();
    };
    $.fn.fadeTabs = $.idTabs.extend(fade);
    $(".tabs").fadeTabs();
    
    //NAVIGATION RESPONSIVE
    $('#nav-Responsive').click(function(e){
	$('#nav-Responsive').toggleClass('closeIcon');
	$('#nav-header').toggleClass('navOn');
	$('#overlay').toggleClass('overlayOn');
	e.preventDefault();
	});
	//AJOUT HEADER NAVIGATION EN RESPONSIVE
	if(window.matchMedia("(max-width:2560px)").matches){
		$(window).scroll(function() {
			$div = $('#nav-header > div');
	        if ($(window).scrollTop() < 100) {
		        $div.removeClass('navThin');
		    }else{
		        $div.addClass('navThin');
	        }
		});
	}
	//PARTAGE URL (POP-UP)
	if($('#btn-share').length){
		$('#btn-share').magnificPopup({
			type: 'inline',
			preloader: false,
			focus: '#name',
	
			// When elemened is focused, some mobile browsers in some cases zoom in
			// It looks not nice, so we disable it:
			callbacks: {
				beforeOpen: function() {
					if($(window).width() < 700) {
						this.st.focus = false;
					} else {
						this.st.focus = '#name';
					}
				}
			}
		});
	}
});
//OVERLAY
function aOverlay() {
	$('#overlay').height($('body').height()).fadeIn(300);
	$('#overDiv').fadeIn(300);
}