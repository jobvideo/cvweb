$(function () {
//Slider
      $("#slider").responsiveSlides({
      	timeout:	5000,
        pager: 		true,
        speed:		1000,
        auto:		true
      });

//entry
if(window.matchMedia("(min-width:1025px)").matches) {
		var script1 = document.createElement('script');
		script1.type='text/javascript';
		script1.src ='scripts/entry.js';
	// Insertion dans le DOM de la balise script initiale
	document.getElementsByTagName('head')[0].appendChild(script1);
	}


//Menu responsive	
$("#menu-scroll").click(function (){
$("#nav-top").slideToggle("slow");
});

//Button Up
$('.btn_up').click(function() {
    $('html,body').animate({scrollTop: 0}, 'slow');
  });

  $(window).scroll(function(){
     if($(window).scrollTop()<300){
        $('.btn_up').fadeOut();
     }else{
        $('.btn_up').fadeIn();
     }
  });

//Connexion  
$('.popup-with-form').magnificPopup({
		type: 'inline',
		preloader: false,
		focus: '#name',

		// When elemened is focused, some mobile browsers in some cases zoom in
		// It looks not nice, so we disable it:
		callbacks: {
			beforeOpen: function() {
				if($(window).width() < 700) {
					this.st.focus = false;
				} else {
					this.st.focus = '#name';
				}
			}
		}
	});
	
//Tabs
var fade = function(id,s){
  s.tabs.removeClass(s.activate);
  s.tab(id).addClass(s.activate);
  s.items.fadeOut();
  s.item(id).fadeIn();
  return false;
};
$.fn.fadeTabs = $.idTabs.extend(fade);
$(".tabs").fadeTabs();


//Custom-Select
//$('.selmenu').customSelect();

});