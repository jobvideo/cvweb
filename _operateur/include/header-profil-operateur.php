<section id="featured" class="featured featured-inscription clearfix">
   <article id="intro-profil" role="section" class="intro-profil clearfix">
    <div id="image-featured" class="image-featured">
        <h3>bonjour, <?php echo $nom; ?></h3>
			<h1>Bienvenue dans votre espace</h1>
			<p>La fonction d’administration du site rend celui-ci dynamique, réactif et vivant. Vous avez maintenant accès à toutes les rubriques qui sont susceptibles d’être mises à jour, modifiées, validées ou gérées. Si vous rencontrez des difficultés d’accès, veuillez contacter l’Administrateur <a href="">ici</a>.</p>
    </div>
   </article>
</section>
<section id="profil" role="section" class="featured-profil-entreprise clearfix">
	<div id="cv" class="cv clearfix">