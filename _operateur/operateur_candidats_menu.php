<?php include('include/header-profil-operateur.php');?>

		<div id="cv-tabs" class="cv-tabs">

			<div class="cv-tabs-inner clearfix">
				<a href="./?p=editer_profil&session=<?php echo $session ?>#tabs-4" class="btn edit" title="Modifier son profil">Editer son profil</a>

				<ul id="tabs" class="tabs clearfix">
					<li><a class="tab1" id="#tabs-1"id="firstonglet" href="./?<?php  echo $action?>=operateur_profil&<?php  echo $link?>#tabs-1" >
							Mon Profil </a></li>
					<li><a class="tab8"  id="#tabs-8" href="./?<?php  echo $action?>=liste_candidats&<?php  echo $link?>#tabs-8" >
							Candidats   </a></li>
					<li><a class="tab9 activate" id="#tabs-9"  href="./?<?php  echo $action?>=quest&<?php  echo $link?>#tabs-9"  >
							Synthèse  </a></li>
					<li><a class="tab10"  id="#tabs-10" href="javascript:popup('changer_pw.php?admin_user_id=<?php  echo $admin_user_id;?>&<?php  echo $link?>#tabs-10',475,250)" class="menutext">
							Password		</a></li>
				</ul>

			</div>

			<div id="content-tab" class="content-tab">

				<div id="tabs-"<?php echo $tab_num?> class="clearfix tab consult">
 
 <!-- !-->
 
<?php
//---> Rubrique valide ?
$rubrique_id = getRubriqueId($connexion, "operateurs");

//---> Tester la session et importer les variables : $select, $mod, $insert, $delete
//     relatives aux privil�ges de l'utilisateur et de la rubrique en cours
include "../include/session_test.php";

include "../include/operateur_candidats.php";     //---> Les fonctions du module candidat
//---> Utiliser le module pagination
$operateur_id = $_SESSION['operateur_id'];

	//---> Proc�dure de suppression
	if (isset($_GET['supprimer'])  && $delete == 'Y') {
		candidat_supprimer($_GET['supprimer']);
	} //Fsi

	if (isset($_GET['visibilite'])  && $delete == 'Y') {
		candidat_supprimer($_GET['supprimer']);
	} //Fsi

/*
if (isset($_POST['id']) && $mod == 'Y') {
    //---> Proc�dure de modification "visible"
    if (isset($_POST['visible']))
        candidat_visible($_POST['visible'], $_POST['id']);
    else
        candidat_visible(NULL, $_POST['id']);  // Tous � faux
*/


//---> Proc�dure de modification "une"
//} //Fsi

/* * *******************************************************************************************************
  Gestion de la pagination
 * ******************************************************************************************************** */
//---> Cr�er un objet de pagination sans condition SQL sur la table
$p = new CAdminPagination($connexion, "candidat", "candidat_operateur_id='$operateur_id'", 10, "candidat_id");
$p->writeJavaScript();    //---> Générer le code JavaScript correspondant
  $res = $p->makeButtons($action);    //---> Afficher les bouttons

?>
<script type="text/javascript" language="javascript">

    function verif()
    {
        var msg = "Voulez reellement appliquer les changements demandes (modification + suppression) ?";
        if (confirm(msg))
            document.pagination_tab.submit();
    } //Fin appliquer

    function ajouter()
    {

        popup('operateur_candidats_add.php?<?php echo $link ?>&operateur_id=<?php echo $operateur_id ?>&admin_user_id=<?php echo $admin_user_id ?>', 625, 750, true);
    } //Fin ajouter

</script>

<div class="bloc-titre">
<h2>Liste de mes candidats</h2>
<!--
	LINK LISTE OPÉRATEURS
	 <span class="bleu"> | <a href="./?<?php echo $action ?>=operateurs&<?php echo $link ?>" >  operateur  </a> </span> -->
</div>
<section class="cv-content cv-ope clearfix">
	<form name="pagination_tab" method="post" action="">

	 <?php
        $i = 0;
        $n = 0;
        $nbre = $res->rowCount();

        while ($row = $res->fetch()) {
            $i++;
            $n++;
            $disabled = ($mod != 'Y') ? "disabled" : "";
            $color = ($i % 2 != 0) ? "#EFEFEF" : "#E9E9E9";
            $session = $_GET["session"];
            $candidat_id = $row['candidat_id'];
            $candidat_nom = affichage($row['candidat_nom'], "---");

            if ($row['candidat_photo'] != 'img_vide.gif') {
                $candidat_photo = "../common/Images/candidat/" . stripslashes($row['candidat_photo']);
            } else {
                $candidat_photo = "../common/Images/candidat/img_vide.gif";
            }

            $candidat_visible = ($row['candidat_visible'] == 'Y') ? "CHECKED" : "";
            //-->les accesssoires de chaque candidat
            $sql2 = " SELECT DISTINCT experience_id FROM experience WHERE candidat_id = $candidat_id ";
            $res2 = $connexion->query($sql2);
            $nb_exp = $res2->rowCount();

            $sql3 = " SELECT DISTINCT graphic_competence_id FROM graphic_competence WHERE candidat_id = $candidat_id ";
            $res3 = $connexion->query($sql3);
            $nb_comp = $res3->rowCount();

            $sql4 = " SELECT DISTINCT partie_synthese_id 	 FROM question_type   ";
            $res4 = $connexion->query($sql4);
            $nb_synt = $res4->rowCount();
		?>
		<article class="cv-content-inner cv-content-inner-ope">
			<div class="cv-file clearfix">
				<div class="cv-contener cv-contener-ope clearfix">
					<figure id="portrait" class="portrait">
						<img src="<?php echo $candidat_photo ; ?>" alt="portrait-baptiste"/>
					</figure>
					<div class="cv-text">
						<div class="cv-col">
							<h2><strong><?php echo $candidat_nom ; ?></strong></h2>
						</div>
						<div class="cv-col">
							<ul class="op-candidat-exp">
								<li>Expériences<strong><a href="./?<?php echo $action ?>=experience_candidats&<?php echo $link ?>&candidat_id=<?php echo $candidat_id ?>&operateur_id=<?php echo $operateur_id ?>" ><?php echo $nb_exp; ?></a></strong></li>
								<li>Compétences<strong><a href="./?<?php echo $action ?>=competence_candidats&<?php echo $link ?>&candidat_id=<?php echo $candidat_id ?>&operateur_id=<?php echo $operateur_id ?>" ><?php echo $nb_comp; ?></a></strong></li>
								<li>Synthèse<strong><a href="./?<?php echo $action ?>=synthese_candidats&<?php echo $link ?>&candidat_id=<?php echo $candidat_id ?>&operateur_id=<?php echo $operateur_id ?>" > <?php echo $nb_synt; ?></a></strong></li>
							</ul>
						</div>
					</div>
				</div>
				<div class="btnOpe clearfix">
					<div class="btnOpeContent btnOpeView">
					<a href="./?<?php echo $action ?>=liste_candidats&<?php echo $link ?>&candidat_id=<?php echo $candidat_id ?>&operateur_id=<?php echo $operateur_id ?>&visibilite=<?php echo $candidat_id ?>" >
<!--
						<input type="checkbox" id="visibilite" value="visibilite" name="visibilite">
						<label class="view-on" for="visibilite"><span></span><p>
-->
						visibilite</a><!-- </p></label> -->
					</div>
					<div class="btnOpeContent btnOpeDelete">
					<a href="./?<?php echo $action ?>=liste_candidats&<?php echo $link ?>&candidat_id=<?php echo $candidat_id ?>&operateur_id=<?php echo $operateur_id ?>&supprimer=<?php echo $candidat_id ?>" >
<!--
						<input type="checkbox" id="supprimer" value="supprimer" name="supprimer">
						<label class="delete-candidat" for="supprimer"><span></span><p>
-->
						supprimer</a><!-- </p></label> -->
					</div>
					<div class="btnOpeContent btnOpeModif">
						<a href="javascript:popup('operateur_candidats_add.php?<?php echo $link ?>&admin_user_id=<?php echo $admin_user_id ?>&candidat_id=<?php echo $row['candidat_id'] ?>&operateur_id=<?php echo $operateur_id ?>',625,750,true);" >Modifier les données candidat</a>
					</div>
					<div class="btnOpeContent btnOpeVoir">
						<a class="button voir-profil" title="Cliquer pour voir le profil" href="./?p=voir&cnd=<?php echo $candidat_id;  ?>&admin_user_id=<?php echo $admin_user_id ?>&operateur_id=<?php echo $operateur_id ?>&session=<?php echo $idses?>" target="_blank">voir le profil</a>
					</div>
				</div>
		</div>

		</article>
		<?php
		}
		?>
		<input type="button" name="wp-submit" id="wp-submit" class="wp-submit button-primary" value="Ajouter un candidat" onClick="javascript:  ajouter();">
	</form>
</section>

 <!-- !-->
				</div>
			</div>

		</div>
	</div>
</section>
 