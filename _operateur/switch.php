<?php
/*************************************************************************************************
                           Chaque cas correpond à une table Msql précise
****************************************************************************************************/
if (!isset($_GET[$action]))
  $_GET[$action] = "accueil";
$ecoute_menu = "closed";

$precedant = $_SERVER['REQUEST_URI'];

switch($_GET[$action])
{

/************************   ****************************/
    case "entreprises"     		  	:$rubrique = "entreprises"; $tab_num = 1; $include_filename = "entreprises_menu.php"; break;
	case "operateurs"     		  	:$rubrique = "operateurs" ; $tab_num = 1;$include_filename = "operateurs_menu.php"; break;
	case "liste_candidats"      	:$rubrique = "operateurs" ; $tab_num = 8; $include_filename = "operateur_candidats_menu.php";break;
	case "experience_candidats"     :$rubrique = "operateurs" ; $tab_num = 8; $include_filename = "experience_menu.php";break;
	case "competence_candidats"     :$rubrique = "operateurs" ; $tab_num = 8; $include_filename = "graphic_competence_menu.php";break;
	case "synthese_candidats"      	:$rubrique = "operateurs" ; $tab_num = 9;  $include_filename = "synthese_candidats_menu.php";break;
	case "question_type_candidat"   :$rubrique = "operateurs" ; $tab_num = 8; $include_filename = "question_type_candidat_menu.php";break;
	case "question_candidat"      	:$rubrique = "operateurs" ; $tab_num = 8;$include_filename = "question_candidat_menu.php";break;
	case "voir"      				:$rubrique = "operateurs" ; $tab_num = 8;   $include_filename = "voir_profil_complet.php";  break;

/************************* synthese **************************/
 	case "quest"      			:$rubrique = "partie_synthese" ; $tab_num = 9; $include_filename = "partie_synthese_menu.php";break;
 	case "question_type"      	:$rubrique = "partie_synthese" ; $tab_num = 9; $include_filename = "question_type_menu.php";break;
 	case "question"      		:$rubrique = "partie_synthese" ; $tab_num = 9; $include_filename = "question_menu.php";break;
 	case "reponse_questions"    :$rubrique = "partie_synthese" ; $tab_num = 9; $include_filename = "reponse_question_menu.php";break;

/********************* fin synthese *************************************/



	case "change_session"                 :$rubrique = "admin_user" ;  $include_filename = "change_session_menu.php"; break;
	case "admin_user"				      : $rubrique = "admin_user" ; $include_filename = "admin_user_menu.php"; break;
	case "parametres"					  :$rubrique = "parametres" ;  $include_filename = "parametres_menu.php"; break;


	default 						      :$rubrique = "accueil" ; $tab_num = 1; $include_filename = "accueil.php";
} //Fin switch
?>