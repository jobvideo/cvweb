<?php include('include/header-profil-operateur.php');?>

		<div id="cv-tabs" class="cv-tabs">

			<div class="cv-tabs-inner clearfix">
				<a href="./?p=editer_profil&session=<?php echo $session ?>#tabs-4" class="btn edit" title="Modifier son profil">Editer son profil</a>

				<ul id="tabs" class="tabs clearfix">
					<li><a class="tab1" id="#tabs-1"id="firstonglet" href="./?<?php  echo $action?>=operateur_profil&<?php  echo $link?>#tabs-1" >
							Mon Profil </a></li>
					<li><a class="tab8"  id="#tabs-8" href="./?<?php  echo $action?>=liste_candidats&<?php  echo $link?>#tabs-8" >
							Candidats   </a></li>
					<li><a class="tab9 activate" id="#tabs-9"  href="./?<?php  echo $action?>=quest&<?php  echo $link?>#tabs-9"  >
							Synth�se  </a></li>
					<li><a class="tab10"  id="#tabs-10" href="javascript:popup('changer_pw.php?admin_user_id=<?php  echo $admin_user_id;?>&<?php  echo $link?>#tabs-10',475,250)" class="menutext">
							Password		</a></li>
				</ul>

			</div>

			<div id="content-tab" class="content-tab">

				<div id="tabs-"<?php echo $tab_num?> class="clearfix tab consult">
 
					   <!-- !-->
					   
<?php 
   //---> Rubrique valide ?
   $rubrique_id = getRubriqueId($connexion,"admin_user");
   
   //---> Tester la session et importer les variables : $select, $mod, $insert, $delete
   //     relatives aux privil�ges de l'utilisateur et de la rubrique en cours
   include "../include/session_test.php";
   
   include "../include/admin_user.php";       //---> Les fonctions du module admin_user   	
    
            //---> Utiliser le module pagination	
  
   //---> Proc�dure de suppression
   if (isset($_POST['supprimer']) && count($_POST['supprimer']) > 0 && $delete=='Y')
   {
	 admin_user_supprimer($_POST['supprimer']);
	 
	 //---> Supprimer les utilsateurs (pas la peine)
	 //CAdminPagination::deleteElements("admin_user", $_POST['supprimer']);
   } //Fsi
  $cond="admin_user_id <> 8";
   if (isset($_POST['id']) && $mod=='Y')
   {
     //---> Proc�dure de modification "actif"
     if (isset($_POST['actif']))
	   admin_user_actif($_POST['actif'],$_POST['id']);
	 else
	   admin_user_actif(NULL,$_POST['id']);  //Tous � faux
   } //Fsi

  /*********************************************************************************************************
                                            Gestion de la pagination
  **********************************************************************************************************/
  //---> Cr�er un objet de pagination sans une condition SQL sur la table
  $p = new CAdminPagination($connexion,"admin_user", $cond, 9, "admin_user_nom");
  $p->writeJavaScript();    //---> G�n�rer le code JavaScript correspondant   
?>
<script type="text/javascript" language="javascript">
<!--
  function verif()
  {
    var msg = "Voulez vous r�ellement appliquer les changements demand�s (modification + suppression)?"
	if (confirm(msg))
	  document.pagination_tab.submit();
  } //Fin appliquer
  
  function ajouter()
  {
    popup_scroll('admin_user_add.php?<?php  echo $link?>&admin_user_id=<?php  echo $admin_user_id?>',825,520,true,0,0);
  } //Fin ajouter
-->
</script>
<table width="100%" align="center" border="0" cellpadding="0" cellspacing="0">
<tr valign="top">
  <td colspan="2" height="10"></td>
</tr>
<tr valign="top" align="left">
  <td width="15" height="25"></td>
  <td>
    <span class="titre">Administrateurs</span>
  </td>
</tr>
<tr>
  <td colspan="2" height="2" bgcolor="#FF0000"></td>
</tr>
<tr valign="top">
  <td height="25"></td>
  <td >
    <img src="./images/flnoir.gif">
	Liste des administrateurs du site
  </td>
</tr>
<tr valign="top">
  <td height="15"></td>
  <td></td>
</tr>
<tr valign="top">
  <td width="15" height="25"></td>
  <td>
    <!-- D�but du tableau avec un syst�me de pagination -->
    <table width="95%" border="0" align="center" cellpadding="0" cellspacing="0">
    <tr>
	  <td colspan="2">
        <!-- D�but de l'en�te de pagination -->
		<?php
		  $res = $p->makeButtons($action);    //---> Afficher les bouttons    
		?>
		<!-- Fin de l'en�te de pagination -->
	  </td>
	</tr>
	<tr>
	  <td height="5"></td>
    </tr>
	<tr>
	  <td>
	    <?php
		  if($res->rowCount()>0 && $select=='Y') //---> Autorisations suffisantes ?
          {
		?>
	    <form name="pagination_tab" method="post" action="">
		<?php
		  $p->writeForm(FALSE);
		?>		
		<!-- D�but du tableau de contenu -->
		<table width="100%" border="1" align="center" cellpadding="0" cellspacing="0" style="border-color:#A0B0B6">
        <tr>
          <td>
            <table width="100%" align="center" border="0" cellpadding="2" cellspacing="1" >
            <tr class="tcat">
              <td width="25" class="inactif">
			    #
			  </td>
              <td width="70">
			    <a href="<?php  echo $p->makelink('admin_user_nom', $p->courent)?>" >
	              Nom <?php $p->writeArrow('admin_user_nom'); ?>
				</a>
			  </td>
              <td class="inactif">
			    Description
			  </td>
              <td width="50">
	            <a href="<?php  echo $p->makelink('admin_user_date', $p->courent)?>" >
	              Date <?php $p->writeArrow('admin_user_date'); ?>
				</a>				
			  </td>
              <td width="50" class="inactif">
	            Privil�ges
			  </td>	
              <td width="50">
				<a href="<?php  echo $p->makelink('admin_user_actif', $p->courent)?>" >
	              Actif <?php $p->writeArrow('admin_user_actif'); ?>
				</a>			    
			  </td>
	          <?php
	             if($delete=='Y')
	             { 
	          ?>
              <td width="40" class="inactif">
				Effacer
			  </td>
			  <?php
			     }//Fsi
			  ?>			  			  			  
            </tr>
	        <?php
	          $i = 0;
	          while($row=@$res->fetch())
	          {
			    $i++;
                
				//---> On n'a pas le droit de modifier ses propres param�tres
				$b_mod                  = ($admin_user_id==$row['admin_user_id'])? 'N' : $mod;	
		        $b_delete               = ($admin_user_id==$row['admin_user_id'])? 'N' : $delete;
				
			    $disabled            	= ($b_mod!='Y')? "disabled" : "";
				$color                  = ($i%2!=0)? "#EFEFEF" : "#E9E9E9";
				$session                = $_GET["session"];
			    $admin_user_nom         = affichage($row['admin_user_nom'],"---");
				$admin_user_description = portion(affichage($row['admin_user_description'],"---"),100);
				$admin_user_date        = date("d/m/Y",$row['admin_user_date']);
				$admin_user_actif       = ($row['admin_user_actif']=='Y')? "CHECKED" : "";
                $admin_user_pouvoir     = affichage($row['admin_user_pouvoir'],"---");				
			?>
		    <tr bgcolor="<?php  echo $color?>" id         = "<?php  echo $i?>" 
			                          onMouseOver="javascript: hightlight_row(this);"
					                  onMouseOut ="javascript: restore_row   (this,'<?php  echo $color?>');">
              <td height="25" align="left" class="inactif">
			    <?php  echo $p->courent*$p->page+$i?>
				<?php
		          if ($row['admin_user_id']!=$admin_user_id)
		          { //---> Si utilisateur en cours, alors c pas la peine d'afficher l'input
				?>
				<input type="hidden" name="id[]" value="<?php  echo $row['admin_user_id']?>">
				<?php
				  } //Fsi
				?>
			  </td>
              <td>
	          <?php 
				if($b_mod=='Y') 
		        {
		      ?>
                <a href="javascript:popup_scroll('admin_user_add.php?<?php  echo $link?>&admin_user_id=<?php  echo $admin_user_id?>&admin_user_id2=<?php  echo $row['admin_user_id']?>',825,520,true,0,0);" class="menutext">
				  <?php  echo $admin_user_nom?>
				</a>
		      <?php
				} else 
				{
				  echo $admin_user_nom;
				} //Fsi
              ?>
			  </td>
              <td><?php  echo $admin_user_description?></td>
              <td><?php  echo $admin_user_date?></td>
			  <td>
	          <?php   
				   if($b_mod=='Y') 
		           {
		      ?>
				<a href="javascript:popup_scroll('admin_user_privilege.php?<?php  echo $link?>&admin_user_id=<?php  echo $admin_user_id?>&admin_user_id2=<?php  echo $row['admin_user_id']?>',400,515,true,0,0);" class="menutext">
			      <?php  echo $admin_user_pouvoir?>
				</a>
		      <?php
			       } else 
			       {
				     echo $admin_user_pouvoir;
		           } //Fsi ($b_mod=='Y') 		        
              ?>				
              </td>
              <td align="center">
				<input type="Checkbox" name="actif[]" value="<?php  echo $row['admin_user_id']?>" style="color:#666666;" <?php  echo $admin_user_actif?> <?php  echo $disabled?> >
			  </td>
			  <?php
			    if($delete=='Y')
	            { 
	          ?>			  
              <td align="center">
              <?php
			    if($b_delete=='Y')
	            {
	          ?>			    
				<input type="Checkbox" name="supprimer[]" id="supprimer<?php  echo $i?>" value="<?php  echo $row['admin_user_id']?>"  onClick="javascript: restore_row(this.parentNode.parentNode, '<?php  echo $color?>');" >
			  <?php
			    } //Fsi
			  ?>
			  </td>
			  <?php
			    } //Fsi
			  ?>
            </tr>
			<?php
			  } //FTQ
			?>
            </table>
          </td>
        </tr>
        </table>
		<?php
		  } //Fsi
		?>		
		<!-- Fin du tableau de contenu -->
	    </form>		
	  </td>
	</tr>
	<tr>
	  <td height="5"></td>
    </tr>
	<tr>
	  <td align="right">
	    <!-- D�but de la barre des bouttons -->
		<table align="right" border="0" cellpadding="0" cellspacing="0">
		<tr>
		  <td height="22">
		  <?php
            if($insert=='Y')                   //---> Autorisations suffisantes ?
            { 
          ?>
		    <div style="cursor:hand" onClick="javascript: ajouter();">
              <table width="75"  border="0" cellpadding="0" cellspacing="0" class="menutext">
              <tr>
                <td width="6"><img src="./images/boutton-gauche.gif" border="0"></td>
                <td width="401" align="center" style="background-image:url(./images/boutton-fond.gif)">Ajouter</td>
                <td width="11"><img src="./images/boutton-droite.gif" border="0"></td>
              </tr>
              </table>
			</div>
		  <?php
		    } //Fsi
		  ?>		  		  
		  </td>
	      <?php
            if($res->rowCount()>0 && $select=='Y') //---> Autorisations suffisantes ?
            {
		  ?>
		  <td width="15"></td>
		  <td height="22">
		    <div style="cursor:hand" onClick="javascript: verif();">
              <table width="75"  border="0" cellpadding="0" cellspacing="0" class="menutext">
              <tr>
                <td width="6"><img src="./images/boutton-gauche.gif" border="0"></td>
                <td width="401" align="center" style="background-image:url(./images/boutton-fond.gif)">Appliquer</td>
                <td width="11"><img src="./images/boutton-droite.gif" border="0"></td>
              </tr>
              </table>
			</div>
	      <?php
		    } //Fsi
		  ?>						  
		  </td>		  		  
		</tr>
		</table>
	    <!-- Fin de la barrre des bouttons -->
	  </td>
	</tr>	
    </table>
    <!-- Fin du tableau avec un syst�me de pagination -->
  </td>
</tr>
<tr>
  <td height="15"></td>
  <td></td>
</tr>															
</table>
					   					   <!-- !-->


				</div>
			</div>

		</div>
	</div>
</section>
 