<?php include('include/header-profil-operateur.php');?>
		<div id="cv-tabs" class="cv-tabs">
			<div class="cv-tabs-inner clearfix">
				<a href="./?p=editer_profil&session=<?php echo $session ?>#tabs-4" class="btn edit edit-on" title="Modifier son profil">Editer son profil</a>
				<ul id="tabs" class="tabs clearfix">
					<li><a class="tab1" id="firstonglet" href="./?p=profil&session=<?php echo $session ?>#tabs-1" title="Voir votre profil">Votre profil</a></li>
					<li><a class="tab2" id="#tabs-2" href="./?p=profils_consulte&session=<?php echo $session ?>#tabs-2"  title="Historique de vos consultations"><span class="nbre_ex"><?php echo $nb_consult?></span><span class="txtNbre">Profil(s) consulté(s)</span></a></li>
					<li><a class="tab3" id="#tabs-3" href="./?p=profils_achete&session=<?php echo $session ?>#tabs-3"  title="Historique de vos achats"><span class="nbre_ex"><?php echo $nb_acht?></span><span class="txtNbre">Profil(s) acheté(s)</span></a></li>
<!-- 					<li><a class="tab4 activate" id="#tabs-4" href="./?p=editer_profil&session=<?php echo $session ?>#tabs-4"  title="Éditer votre profil">Éditer son profil</a></li> -->
					<li><a class="tab5" id="#tabs-5" href="./?p=rechercher&session=<?php echo $session ?>#tabs-5"  title="Faire une recherche cv multimédia">Rechercher un candidat</a></li>
					<li><a class="tab6" id="#tabs-6" href="./?p=abonner&session=<?php echo $session ?>#tabs-6"  title="S'abonner ou acheter">S'abonner acheter un cv candidat</a></li>
				</ul>
			</div>
			<div id="content-tab" class="content-tab">


				<div id="tabs-4" class="clearfix tab edit">
					<?php include('include/edit-profil-operateur.php');?>
				</div>
			</div>
		</div>
	</div>
</section>
